package view;

import entities.Scheda;

/**
 *
 */
public interface DatiScheda {
    /**
     * @param scheda Scheda corrente.
     */
    void setScheda(Scheda scheda);

    /**
     * 
     * @return scheda corrente
     */
    Scheda getScheda();
}
