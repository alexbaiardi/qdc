package controller;

import java.util.List;

import entities.Coltura;
import entities.Varieta;

/**
 * Gestore delle colture.
 */
public interface GestoreColture {

    /**
     * Metodo per ottenere le colture di un'azienda.
     * @return Lista di colture
     */
    List<Coltura> getColture();

    /**
     * Metodo per ottenere le varietà di una coltura.
     * @param c Coltura
     * @return Lista di varietà
     */
    List<Varieta> getVarieta(Coltura c);

    /**
     * Metodo per ottenere la coltura associata ad un certo codice.
     * @param codice Codice della coltura.
     * @return Coltura
     */
    Coltura getColtura(int codice);

    /**
     * Metodo per ottenere la varietà dati il progressivo e il codice della coltura.
     * @param codiceColtura Codice della coltura
     * @param progVarieta Progressivo della varietà
     * @return Varietà
     */
    Varieta getVarieta(int codiceColtura, int progVarieta);
}
