package view;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import entities.Azienda;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;

public class VisualizzazioneAziendeImpl implements Initializable {

   @FXML
    private ComboBox<String> cbAzienda;

    @FXML
    private TextField tfCodiceSocio;

    @FXML
    private TextField tfPartitaIva;

    @FXML
    private TextField tfResponsabile;

    @FXML
    private TextField tfNome;

    @FXML
    private TextField tfCap;

    @FXML
    private TextField tfProvincia;

    @FXML
    private TextField tfComune;

    @FXML
    private TextField tfCivico;

    @FXML
    private TextField tfVia;

    private List<Azienda> aziende;
    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        GestoreAziende gestoreA = new GestoreAziendeImpl();
        aziende = gestoreA.getAziende();
        aziende.forEach(e -> cbAzienda.getItems().add(e.getNome()));
        cbAzienda.setOnAction(e -> showCompany());
        tfCap.setDisable(true);
        tfCivico.setDisable(true);
        tfCodiceSocio.setDisable(true);
        tfComune.setDisable(true);
        tfNome.setDisable(true);
        tfPartitaIva.setDisable(true);
        tfProvincia.setDisable(true);
        tfResponsabile.setDisable(true);
        tfVia.setDisable(true);
    }
 
    /**
     * 
     */
    @FXML
    private void showCompany() {
        Azienda a = aziende.get(cbAzienda.getSelectionModel().getSelectedIndex());
        tfCodiceSocio.setText(a.getCodiceSocio());
        tfPartitaIva.setText(a.getPartitaiva());
        tfResponsabile.setText(a.getResponsabile());
        tfNome.setText(a.getNome());
        tfCap.setText(a.getCap());
        tfProvincia.setText(a.getProvincia());
        tfComune.setText(a.getComune());
        tfCivico.setText("" + a.getCivico());
        tfVia.setText(a.getVia());
    }
}


