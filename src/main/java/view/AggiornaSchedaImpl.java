package view;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import controller.GestoreColture;
import controller.GestoreColtureImpl;
import controller.GestoreSchede;
import controller.GestoreSchedeImpl;
import entities.Azienda;
import entities.Coltura;
import entities.RigaScheda;
import entities.Scheda;
import entities.Varieta;
import javafx.fxml.Initializable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class AggiornaSchedaImpl implements Initializable {

    @FXML
    private Button btSelect;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colSuperficie;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colVarieta;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colCodiceRiga;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colParticella;

    @FXML
    private ComboBox<String> cbAzienda;

    @FXML
    private ComboBox<String> cbColtura;

    @FXML
    private ComboBox<Integer> cbAnno;

    @FXML
    private ComboBox<String> cbvarieta;

    @FXML
    private TextField txtParticella;

    @FXML
    private TextField txtSuperficie;

    @FXML
    private Button btAggiorna;

    @FXML
    private TableView<TabellaRigheScheda> tbRighe;

    private Azienda azienda;
    private Coltura coltura;
    private Varieta varieta = null;
    private List<Azienda> aziende;
    private List<Coltura> colture;
    private List<Varieta> lVarieta;
    private GestoreColture gestoreC;
    private GestoreSchede gestoreS;
    private Scheda currentScheda;
    private int anno = 0;
    private static final Set<Integer> YEARS = Set.of(2020, 2019, 2018, 2017, 2016);
    private final ObservableList<TabellaRigheScheda> data = FXCollections.observableArrayList();
    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        gestoreS = new GestoreSchedeImpl();
        gestoreC = new GestoreColtureImpl();
        GestoreAziende gestoreA = new GestoreAziendeImpl();
        aziende = gestoreA.getAziende();
        aziende.forEach(e -> cbAzienda.getItems().add(e.getNome()));

        colture = gestoreC.getColture();
        colture.forEach(e -> cbColtura.getItems().add(e.getNome()));

        cbAnno.getItems().addAll(YEARS);
        cbColtura.setDisable(true);
        cbvarieta.setDisable(true);
        cbAnno.setDisable(true);
        btAggiorna.setDisable(true);
        btSelect.setDisable(true);
    }

    private void addRigheTable(final Scheda s) {
        tbRighe.getItems().clear();
        colCodiceRiga.setCellValueFactory(new PropertyValueFactory<>("codiceRiga"));

        colVarieta.setCellValueFactory(new PropertyValueFactory<>("varieta"));

        colParticella.setCellValueFactory(new PropertyValueFactory<>("particella"));

        colSuperficie.setCellValueFactory(new PropertyValueFactory<>("superficie"));

        final List<RigaScheda> righe = s.getRighe();
        if (!righe.isEmpty()) {
            righe.forEach(r -> {
                final TabellaRigheScheda riga = new TabellaRigheScheda(r.getCodiceRiga(), r.getSuperficie(),
                        r.getParticella(),
                        "" + gestoreC.getVarieta(r.getCodicecoltura(), r.getProgvarieta()).getNome());
                data.add(riga);
            });
        }
        this.tbRighe.setItems(data);
    }

    /**
     * 
     */
    @FXML
    public void aggiornaScheda() {
        if (checkField()) {
           gestoreS.aggiungiRiga(new RigaScheda(currentScheda.getCodiceScheda(), Double.parseDouble(txtSuperficie.getText()), txtParticella.getText(), coltura.getCodiceColtura(), varieta.getProgVarieta()));
           tbRighe.getItems().clear();
           currentScheda = gestoreS.getSchede(azienda).stream().filter(az -> az.getCodiceScheda() == currentScheda.getCodiceScheda()).findFirst().get();
           addRigheTable(currentScheda);
        }
    }

    /**
     * 
     * @return
     */
    private boolean checkField() {
        String errorMessage = "";
        if (txtParticella.getText().isBlank() || txtSuperficie.getText().isBlank() || varieta == null) {
            errorMessage = "Impossibile lasciare campi vuoti!";
            System.out.println(errorMessage);
        }
        return errorMessage.equals("");
    }
    /**
     * 
     */
    @FXML
    public void selezionaScheda() {
        final Scheda newScheda = new Scheda(azienda.getCodiceSocio(), coltura.getCodiceColtura(), anno);
        if (!gestoreS.getCodiceScheda(newScheda).isEmpty()) {
            int codice =  gestoreS.getCodiceScheda(newScheda).get();
            currentScheda = gestoreS.getSchede(azienda).stream().filter(az -> az.getCodiceScheda() == codice).findFirst().get();
            addRigheTable(currentScheda);
            lVarieta = new ArrayList<Varieta>();
            lVarieta = gestoreC.getVarieta(coltura);
            cbvarieta.getItems().clear();
            lVarieta.forEach(e -> cbvarieta.getItems().add(e.getNome()));
            cbvarieta.setDisable(false);
            btAggiorna.setDisable(false);
        } else {
            tbRighe.getItems().clear();
            System.out.println("Nessuna Scheda trovata");
            txtParticella.clear();
            txtSuperficie.clear();
            cbvarieta.setDisable(true);
            btSelect.setDisable(true);
        }
    }
    /**
     * 
     * @param event
     */
    @FXML
    public void getAnno(final ActionEvent event) {
        anno = cbAnno.getValue();
        btSelect.setDisable(false);
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void getAzienda(final ActionEvent event) {
        azienda = aziende.get(cbAzienda.getSelectionModel().getSelectedIndex());
        cbColtura.setDisable(false);
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void getColtura(final ActionEvent event) {
        coltura = colture.get(cbColtura.getSelectionModel().getSelectedIndex());
        cbAnno.setDisable(false);
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void cbGetVarieta(final ActionEvent event) {
        try {
            varieta = lVarieta.get(cbvarieta.getSelectionModel().getSelectedIndex());
        } catch (Exception e) {
        }
    }
}
