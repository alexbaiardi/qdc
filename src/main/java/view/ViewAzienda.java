package view;

import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import controller.GestoreColture;
import controller.GestoreColtureImpl;
import controller.GestoreSchede;
import controller.GestoreSchedeImpl;
import entities.Azienda;
import entities.Scheda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Classe che implementa la grafica dell'azienda.
 */
public class ViewAzienda extends AbstractAziendaSceneController implements Initializable {

    private GestoreSchede gestoreSchede = new GestoreSchedeImpl();
    private GestoreColture gestoreColture = new GestoreColtureImpl();

    @FXML
    private TableColumn<TabellaViewAzienda, String> colInsTrattamento;

    @FXML
    private TableColumn<TabellaViewAzienda, String> colColtura;

    @FXML
    private MenuButton mbSelezionaOperazione;

    @FXML
    private TableColumn<TabellaViewAzienda, String> colVisTrattamenti;

    @FXML
    private TableColumn<TabellaViewAzienda, String> colScheda;

    @FXML
    private TableView<TabellaViewAzienda> tbSchede;

    @FXML
    private ComboBox<Integer> cbAnno;

    private List<Scheda> schede = new LinkedList<>();

    private final ObservableList<TabellaViewAzienda> data = FXCollections.observableArrayList();

    /**
     * 
     */
    @Override
    public void setAzienda(final Azienda azienda) {
        super.setAzienda(azienda);
        this.initializeDatiAzienda();
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void inserisciAcquisto(final ActionEvent event) {
        this.openSceneAzienda("inserimentoAcquisto.fxml");
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void visualizzaAcquisti(final ActionEvent event) {
        this.openSceneAzienda("visualizzazioneAcquisti.fxml");
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void visualizzaMovimenti(final ActionEvent event) {
        this.openSceneAzienda("visualizzazioneMovimenti.fxml");
    }

    /**
     * 
     * @param event
     */
    @FXML
    public void visualizzaGiacenze(final ActionEvent event) {
        this.openSceneAzienda("visualizzazioneGiacenze.fxml");
    }

    /**
     * 
     * @param event
     */
    @FXML
    void cercaPerAnno(final ActionEvent event) {
        final int anno = this.cbAnno.getValue();
        final List<Scheda> schede = this.schede.stream()
                .filter(s -> s.getAnno() == anno).collect(Collectors.toList());
        if (!schede.isEmpty()) {
            schede.forEach(s -> {
                final Button btVisualizzaScheda = new Button("visualizza");
                btVisualizzaScheda.setOnAction(e -> {
                    openSceneScheda("visualizzazioneScheda.fxml", s);
                });
                final Button btInserisciTrattamento = new Button("inserisci");
                btInserisciTrattamento.setOnAction(e -> {
                    openSceneScheda("inserimentoTrattamento.fxml", s);
                });
                final Button btVisualizzaTrattamenti = new Button("visualizza");
                btVisualizzaTrattamenti.setOnAction(e -> {
                    openSceneScheda("visualizzazioneTrattamenti.fxml", s);
                });
                final TabellaViewAzienda datiTabella = new TabellaViewAzienda(
                        this.gestoreColture.getColtura(s.getCodiceColtura()).getNome(), btVisualizzaScheda,
                        btInserisciTrattamento, btVisualizzaTrattamenti);
                data.add(datiTabella);
            });
        }
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
    }

    private void initializeDatiAzienda() {
        colColtura.setCellValueFactory(new PropertyValueFactory<>("coltura"));

        colScheda.setCellValueFactory(new PropertyValueFactory<>("visualizzaSchede"));

        colInsTrattamento.setCellValueFactory(new PropertyValueFactory<>("inserisciTrattamento"));

        colVisTrattamenti.setCellValueFactory(new PropertyValueFactory<>("visualizzaTrattamenti"));

        schede = this.gestoreSchede.getSchede(this.getAzienda());

        final ObservableList<Integer> anni = FXCollections.observableArrayList();
        schede.forEach(s -> anni.add(s.getAnno()));
        tbSchede.setItems(data);
        cbAnno.setItems(anni);
        cbAnno.getSelectionModel().select(Collections.max(anni));
    }

    private void openSceneAzienda(final String scene) {
        final SceneController controller = this.getSceneFactory().openNewScene(scene);
        controller.setSceneFactory(this.getSceneFactory());
        final DatiAzienda datiAzienda = (DatiAzienda) controller;
        datiAzienda.setAzienda(this.getAzienda());
    }

    private void openSceneScheda(final String scene, final Scheda scheda) {
        final SceneController controller = this.getSceneFactory().openNewScene(scene);
        controller.setSceneFactory(this.getSceneFactory());
        final DatiScheda datiScheda = (DatiScheda) controller;
        datiScheda.setScheda(scheda);
    }
}
