package controller;

import java.util.List;

import entities.Acquisto;
import entities.Azienda;

/**
 * Gestore degli acquisti.
 */
public interface GestoreAcquisti {

    /**
     * Metodo per ottenere una lista degli acquisti di un'azienda.
     * @param az Azienda
     * @return Lista di acquisti
     */
    List<Acquisto> getAcquisti(Azienda az);

    /**
     * Inserimento di un acquisto di un'azienda.
     * @param a Azienda
     */
    void inserisciAcquisto(Acquisto a);

}
