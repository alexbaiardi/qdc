package entities;

/**
 *
 */
public class Azienda {
    private String codiceSocio;
    private String partitaIVA;
    private String nome;
    private String responsabile;
    private String cap;
    private String provincia;
    private String comune;
    private String via;
    private int civico;


    public Azienda(final String codiceSocio, final String partitaIVA, final String nome, final String responsabile, final String cap,
            final String provincia, final String comune, final String via, final int civico) {
        this.codiceSocio = codiceSocio;
        this.partitaIVA = partitaIVA;
        this.nome = nome;
        this.responsabile = responsabile;
        this.cap = cap;
        this.provincia = provincia;
        this.comune = comune;
        this.via = via;
        this.civico = civico;
    }

    public Azienda(final String codiceSocio) {
        this.codiceSocio = codiceSocio;
    }

    /**
     * 
     * @return Codice identificativo dell'azienda.
     */
    public String getCodiceSocio() {
        return codiceSocio;
    }

    /**
     * @return Partita Iva
     */
    public String getPartitaiva() {
        return partitaIVA;
    }

    /**
     * 
     * @return Nome dell'azienda
     */
    public String getNome() {
        return nome;
    }

    /**
     * 
     * @return Nome del responsabile.
     */
    public String getResponsabile() {
        return responsabile;
    }


    /**
     * 
     * @return CAP
     */
    public String getCap() {
        return cap;
    }


    /**
     * 
     * @return provincia
     */
    public String getProvincia() {
        return provincia;
    }



    /**
     * 
     * @return comune
     */
    public String getComune() {
        return comune;
    }


    /**
     * 
     * @return via
     */
    public String getVia() {
        return via;
    }


    /**
     * 
     * @return numero civico
     */
    public int getCivico() {
        return civico;
    }

}
