package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Azienda;
import entities.Movimento;
import entities.Prodotto;

/**
 *
 */
public class GestoreMovimentiImpl implements GestoreMovimenti {

    private DBManager dataSource;

    /**
     * 
     */
    public GestoreMovimentiImpl() {
        this.dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Movimento> getMovimentiAzienda(final Azienda a) {
        List<Movimento> movimenti = new ArrayList<Movimento>();
        final String query = "SELECT nomeProdotto,quantita,data FROM ACQUISTI A JOIN COMPOSIZIONE_ACQUISTI CA ON A.codiceAcquisto = CA.codiceAcquisto "
                + "JOIN PRODOTTI P ON P.nome=CA.nomeProdotto WHERE A.codiceAzienda = '" + a.getCodiceSocio()
                + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String nomeProdotto = res.getString("nomeProdotto");
                final Double quantita = res.getDouble("quantita");
                final Date data = res.getDate("data");
                movimenti.add(new Movimento(nomeProdotto, quantita, data));
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String query2 = "SELECT nomeProdotto,quantitaProdotto,data FROM TRATTAMENTI A WHERE A.codiceAzienda = '"
                + a.getCodiceSocio() + "'";
        final ResultSet res2 = dataSource.executeQuery(query2);
        try {
            while (res2.next()) {
                final String nomeProdotto = res2.getString("nomeProdotto");
                final Double quantita = -res2.getDouble("quantitaProdotto");
                final Date data = res2.getDate("data");
                movimenti.add(new Movimento(nomeProdotto, quantita, data));
            }
            res2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movimenti;
    }

    /**
     * 
     */
    @Override
    public Map<String, Double> getGiacenze(final Azienda a, final LocalDate data) {
        final Map<String, Double> giacenze = new HashMap<String, Double>();
        final String query = "SELECT P.nome,(COALESCE(SUM(CA.quantita),0)-COALESCE(SUM(T.quantitaProdotto),0)) AS giacenza FROM PRODOTTI P JOIN COMPOSIZIONE_ACQUISTI CA ON P.nome = CA.nomeProdotto JOIN ACQUISTI A ON CA.codiceAcquisto = A.codiceAcquisto"
                + " LEFT JOIN TRATTAMENTI T ON P.nome = T.nomeProdotto WHERE A.codiceAzienda='" + a.getCodiceSocio()
                + "' AND A.data<='" + Date.valueOf(data) + "' AND (T.data<='" + Date.valueOf(data) + "' OR T.data IS NULL) GROUP BY P.nome";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String nome = res.getString("nome");
                final Double giacenza = res.getDouble("giacenza");
                giacenze.put(nome, giacenza);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return giacenze;
    }

    /**
     * 
     */
    @Override
    public List<Movimento> getMovimentiAziendabyProd(final Azienda a, final Prodotto p) {
        final List<Movimento> movimentiProd = new ArrayList<Movimento>();
        final String nomeProdotto = p.getNome();
        final String query = "SELECT CA.quantita,A.data FROM COMPOSIZIONE_ACQUISTI CA JOIN ACQUISTI A ON CA.codiceAcquisto = A.codiceAcquisto WHERE A.codiceAzienda = '"
                + a.getCodiceSocio() + "' AND CA.nomeProdotto = '" + p.getNome() + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final Double quantita = res.getDouble("quantita");
                final Date data = res.getDate("data");
                final Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimentiProd.add(mov);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String query2 = "SELECT quantitaProdotto,data FROM TRATTAMENTI WHERE codiceAzienda = '"
                + a.getCodiceSocio() + "'" + "AND nomeProdotto = '" + p.getNome() + "'";
        final ResultSet res2 = dataSource.executeQuery(query2);
        try {
            while (res2.next()) {
                final Double quantita = res2.getDouble("quantitaProdotto");
                final Date data = res2.getDate("data");
                final Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimentiProd.add(mov);
            }
            res2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movimentiProd;
    }

    /**
     * 
     */
    @Override
    public List<Movimento> getMovimentiAziendabyAnno(final Azienda a, final int anno) {
        final List<Movimento> movimentiProd = new ArrayList<Movimento>();
        final String query = "SELECT CA.quantita,A.data,CA.nomeProdotto FROM COMPOSIZIONE_ACQUISTI CA JOIN ACQUISTI A ON CA.codiceAcquisto = A.codiceAcquisto WHERE A.codiceAzienda = '"
                + a.getCodiceSocio() + "' AND year(A.data) = " + anno;
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final Double quantita = res.getDouble("quantita");
                final Date data = res.getDate("data");
                final String nomeProdotto = res.getString("nomeProdotto");
                Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimentiProd.add(mov);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String query2 = "SELECT quantitaProdotto,data FROM TRATTAMENTI WHERE codiceAzienda = '"
                + a.getCodiceSocio() + "' AND year(data) = " + anno;
        final ResultSet res2 = dataSource.executeQuery(query2);
        try {
            while (res2.next()) {
                final Double quantita = res2.getDouble("quantita") * -1;
                final Date data = res2.getDate("data");
                final String nomeProdotto = res2.getString("nomeProdotto");
                Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimentiProd.add(mov);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movimentiProd;
    }

    /**
     * 
     */
    @Override
    public List<Movimento> getMovimentiAziendabyProdAndAnno(final Azienda a, final Prodotto p, final int anno) {
        final List<Movimento> movimenti = new ArrayList<Movimento>();
        final String query = "SELECT CA.quantita,A.data,CA.nomeProdotto FROM COMPOSIZIONE_ACQUISTI CA JOIN ACQUISTI A ON CA.codiceAcquisto = A.codiceAcquisto WHERE A.codiceAzienda = '"
                + a.getCodiceSocio() + "' AND year(A.data) = " + anno + " AND CA.nomeProdotto = '" + p.getNome() + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final Double quantita = res.getDouble("quantita");
                final Date data = res.getDate("data");
                final String nomeProdotto = res.getString("nomeProdotto");
                Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimenti.add(mov);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String query2 = "SELECT quantitaProdotto,data,nomeProdotto FROM TRATTAMENTI WHERE codiceAzienda = '"
                + a.getCodiceSocio() + "' AND year(data) = " + anno + " AND nomeProdotto = '" + p.getNome() + "'";
        final ResultSet res2 = dataSource.executeQuery(query2);
        try {
            while (res2.next()) {
                final Double quantita = res2.getDouble("quantitaProdotto");
                final Date data = res2.getDate("data");
                final String nomeProdotto = res2.getString("nomeProdotto");
                final Movimento mov = new Movimento(nomeProdotto, quantita, data);
                movimenti.add(mov);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movimenti;
    }

    /**
     * 
     */
    @Override
    public double getGiacenzeProdotto(final Azienda a, final Prodotto p, final LocalDate data) {
        return getGiacenze(a, data).get(p.getNome());
    }
}
