package view;

import entities.Scheda;

/**
 * 
 */
public abstract class AbstractSchedaSceneController extends AbstractSceneController implements DatiScheda {

    private Scheda scheda;

    /**
     * 
     */
    @Override
    public void setScheda(final Scheda scheda) {
        this.scheda = scheda;
    }


    /**
     * @return scheda
     */
    public Scheda getScheda() {
        return scheda;
    }
}
