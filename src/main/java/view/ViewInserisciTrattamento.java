package view;

import java.net.URL;
import java.sql.Date;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import controller.GestoreAvversita;
import controller.GestoreAvversitaImpl;
import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import controller.GestoreColture;
import controller.GestoreColtureImpl;
import controller.GestoreTrattamenti;
import controller.GestoreTrattamentiImpl;
import entities.Avversita;
import entities.Azienda;
import entities.Coltura;
import entities.Prodotto;
import entities.RigaScheda;
import entities.Scheda;
import entities.Trattamento;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class ViewInserisciTrattamento extends AbstractSchedaSceneController implements Initializable {

    private static final int NGIORNI = 31;
    private static final int NMESI = 12;

    @FXML
    private TableView<TabellaSelezioneRigheScheda> tbTrattamenti;

    @FXML
    private TableColumn<TabellaSelezioneRigheScheda, String> colVarieta;

    @FXML
    private TableColumn<TabellaSelezioneRigheScheda, String> colCodice;

    @FXML
    private TableColumn<TabellaSelezioneRigheScheda, String> colSeleziona;

    @FXML
    private TableColumn<TabellaSelezioneRigheScheda, String> colSuperficie;

    @FXML
    private TableColumn<TabellaSelezioneRigheScheda, String> colParticella;

    @FXML
    private Button btHomeAzienda;

    @FXML
    private ComboBox<String> cbProdotto;

    @FXML
    private TextField tfQuantita;

    @FXML
    private Button btSalvaInserimento;

    @FXML
    private ComboBox<Integer> cbMese;

    @FXML
    private TextField tfSuperficie;

    @FXML
    private ComboBox<String> cbAvversita;

    @FXML
    private ComboBox<Integer> cbAnno;

    @FXML
    private ComboBox<Integer> cbGiorno;

    @FXML
    private TextField tfVolume;

    private final GestoreAvversita ga = new GestoreAvversitaImpl();
    private final GestoreColture gestoreColture = new GestoreColtureImpl();

    private final List<Avversita> avversita = new ArrayList<>();

    private final List<Prodotto> prodotti = new ArrayList<>();

    private Coltura coltura;

    private final ObservableList<TabellaSelezioneRigheScheda> datiTabella = FXCollections.observableArrayList();

    /**
     * 
     */
    @Override
    public void setScheda(final Scheda scheda) {
        super.setScheda(scheda);
        final GestoreColture gc = new GestoreColtureImpl();
        this.popolaTabella(scheda.getRighe());
        coltura = gc.getColtura(scheda.getCodiceColtura());
        avversita.addAll(ga.getAvversita(coltura));
        cbAvversita.getItems().addAll(avversita.stream().map(a -> a.getNome()).collect(Collectors.toList()));
    }

    /**
     * 
     */
    @FXML
    public void inserisciTrattamento() {
        Alert a = new Alert(AlertType.INFORMATION);
        String errorMessage = "";
        try {
            double volumeAcqua = 0;
            double quantita = 0;
            LocalDate data = null;
            if (cbAnno.getValue() == null && cbMese.getValue() == null && cbGiorno.getValue() == null) {
                errorMessage = errorMessage + "Selezionare una data valida\n";
            } else {
                data = LocalDate.of(cbAnno.getValue(), cbMese.getValue(), cbGiorno.getValue());
            }
            if (cbAvversita.getValue().isBlank()) {
                errorMessage = errorMessage + "Selezionare una avversità\n";
            }
            if (cbProdotto.getValue().isBlank()) {
                errorMessage = errorMessage + "Selezionare un prodotto\n";
            }
            if (tfQuantita.getText().isBlank()) {
                errorMessage = errorMessage + "Inserire la quantità\n";
            } else {
                quantita = Double.parseDouble(tfQuantita.getText());
                if (quantita <= 0) {
                    errorMessage = errorMessage + "Inserire una quantità valida\n";
                }
            }
            if (tfVolume.getText().isBlank()) {
                errorMessage = errorMessage + "Inserire il volume d'acqua\n";
            } else {
                volumeAcqua = Double.parseDouble(tfVolume.getText());
                if (quantita <= 0) {
                    errorMessage = errorMessage + "Inserire un volume d'acqua valido\n";
                }
            }
            if (datiTabella.stream().filter(e -> e.isChecked()).map(r -> r.getCodiceRiga()).collect(Collectors.toList())
                    .isEmpty()) {
                errorMessage = errorMessage + "Selezionare righe scheda";
            }

            if (errorMessage.isBlank()) {
                final Trattamento t = new Trattamento(Date.valueOf(data), volumeAcqua, quantita,
                        prodotti.get(cbProdotto.getItems().indexOf(cbProdotto.getValue())).getNome(),
                        this.getScheda().getCodiceAzienda(),
                        this.avversita.get(cbAvversita.getItems().indexOf(cbAvversita.getValue())).getCodice());
                t.setIdScheda(this.getScheda().getCodiceScheda());
                datiTabella.stream().filter(e -> e.isChecked()).map(r -> r.getCodiceRiga()).collect(Collectors.toList())
                        .forEach(id -> t.inserisciRiga(id));
                final GestoreTrattamenti gt = new GestoreTrattamentiImpl();
                gt.inserisciTrattamento(t);
                tfQuantita.setText("");
                tfVolume.setText("");
                a.setHeaderText("Trattamento Inserito");
            } else {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText(errorMessage);
            }
        } catch (Exception e) {
            a.setAlertType(AlertType.ERROR);
            a.setHeaderText(errorMessage + e.getMessage());
        } finally {
            a.show();
        }
    }

    /**
     * 
     */
    @FXML
    public void cercaProdotti() {
        LocalDate data;
        try {
            data = LocalDate.of(cbAnno.getValue(), cbMese.getValue(), cbGiorno.getValue());
            final Map<Prodotto, Double> prodottiGiacenze = ga.getProdottiAvversitaColturaAzienda(
                    avversita.get(cbAvversita.getItems().indexOf(cbAvversita.getValue())), coltura, new Azienda(this.getScheda().getCodiceAzienda()), data);
            prodotti.clear();
            prodotti.addAll(prodottiGiacenze.keySet());
            cbProdotto.getItems().clear();
            prodotti.forEach(p -> cbProdotto.getItems().add(p.getNome() + " " + prodottiGiacenze.get(p)));
        } catch (DateTimeException e) {
            final Alert a = new Alert(AlertType.ERROR);
            a.setHeaderText("Data non valida");
            a.show();
        }
    }

    /**
     * 
     */
    @FXML
    public void apriHomeAzienda() {
        GestoreAziende ga = new GestoreAziendeImpl();
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(ga.getAziendaFromPrimaryKey(this.getScheda().getCodiceAzienda()));
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        cbGiorno.getItems().addAll(Stream.iterate(1, e -> e + 1).limit(NGIORNI).collect(Collectors.toList()));
        cbMese.getItems().addAll(Stream.iterate(1, e -> e + 1).limit(NMESI).collect(Collectors.toList()));
        cbAnno.getItems().addAll(Year.now().getValue(), Year.now().getValue() - 1, Year.now().getValue() - 2);
        cbAnno.setValue(LocalDate.now().getYear());
        cbMese.setValue(LocalDate.now().getMonth().getValue());
        cbGiorno.setValue(LocalDate.now().getDayOfMonth());
    }

    private void popolaTabella(final List<RigaScheda> righe) {
        colCodice.setCellValueFactory(new PropertyValueFactory<>("codiceRiga"));
        colVarieta.setCellValueFactory(new PropertyValueFactory<>("varieta"));
        colSuperficie.setCellValueFactory(new PropertyValueFactory<>("superficie"));
        colParticella.setCellValueFactory(new PropertyValueFactory<>("particella"));
        colSeleziona.setCellValueFactory(new PropertyValueFactory<>("seleziona"));
        if (!righe.isEmpty()) {
            righe.forEach(r -> {
                final CheckBox seleziona = new CheckBox();
                seleziona.setOnAction(e -> {
                    this.updateSuperficieSelezionata(tbTrattamenti.getItems().stream().filter(rg -> rg.isChecked())
                            .collect(Collectors.toList()));
                });
                final TabellaSelezioneRigheScheda riga = new TabellaSelezioneRigheScheda(
                        r.getCodiceRiga(), this.gestoreColture
                                .getVarieta(super.getScheda().getCodiceColtura(), r.getProgvarieta()).getNome(),
                        r.getSuperficie(), r.getParticella(), seleziona);
                datiTabella.add(riga);
            });
        }
        tbTrattamenti.setItems(datiTabella);
    }

    private void updateSuperficieSelezionata(final List<TabellaSelezioneRigheScheda> righe) {
        final double superficieSelezionata = righe.stream().map(r -> r.getSuperficie()).reduce((r, r1) -> r + r1).orElse(0.0);
        this.tfSuperficie.setText(String.valueOf(superficieSelezionata));
    }

}
