package view;

import javafx.stage.Stage;

public interface SceneFactory {

    void setStage(Stage stage);

    Stage getStage();

    SceneController openNewScene(String scenePath);

}
