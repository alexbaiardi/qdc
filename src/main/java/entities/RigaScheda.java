package entities;

/**
 * Classe che implementa il concetto di riga di una scheda.
 */
public class RigaScheda {
    private int codiceRiga; 
    private int codiceScheda;
    private double superficie;
    private String particella;
    private int codiceColtura;
    private int progVarieta;

    /**
     * Costruttore.
     * @param codiceScheda Codice della scheda
     * @param superficie Superficie dell'appezzamento
     * @param particella Particella dell'appezzamento
     * @param codiceColtura Codice della coltura
     * @param progVarieta Codice progressivo della varietà
     */
    public RigaScheda(final int codiceScheda, final double superficie, final String particella,
            final int codiceColtura, final int progVarieta) {
        this.codiceScheda = codiceScheda;
        this.superficie = superficie;
        this.particella = particella;
        this.codiceColtura = codiceColtura;
        this.progVarieta = progVarieta;
    }

    public RigaScheda(final int codiceRiga, final int codiceScheda, final double superficie, final String particella, final int codiceColtura,
            final int progVarieta) {
        this(codiceScheda, superficie, particella, codiceColtura, progVarieta);
        this.codiceRiga = codiceRiga;
    }


    /**
     * 
     * @return Codice della scheda.
     */
    public int getCodicescheda() {
        return codiceScheda;
    }

    /**
     * 
     * @return Superficie dell'appezzamento.
     */
    public double getSuperficie() {
        return superficie;
    }

    /**
     * 
     * @return Particella alla quale la riga è riferita.
     */
    public String getParticella() {
        return particella;
    }

    /**
     * 
     * @return Codice della coltura a cui fa riferimento.
     */
    public int getCodicecoltura() {
        return codiceColtura;
    }

    /**
     * 
     * @return Codice progressivo della varietà.
     */
    public int getProgvarieta() {
        return progVarieta;
    }

    /**
     * 
     * @return codice Riga
     */
    public int getCodiceRiga() {
        return codiceRiga;
    }
}
