package entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe che implementa il concetto di scheda.
 */
public class Scheda {

    private int codiceScheda;
    private String codiceAzienda;
    private int codiceColtura;
    private int anno;
    private List<RigaScheda> righe = new ArrayList<RigaScheda>();

    /**
     * Costruttore.
     * 
     * @param codiceScheda  Codice della scheda
     * @param codiceAzienda Codice dell'azienda
     * @param codiceColtura Codice della coltura
     * @param anno          Anno di riferimento
     */
    public Scheda(final String codiceAzienda, final int codiceColtura, final int anno) {
        this.codiceAzienda = codiceAzienda;
        this.codiceColtura = codiceColtura;
        this.anno = anno;
    }

    /**
     * 
     * @return Codice identificativo della scheda.
     */
    public int getCodiceScheda() {
        return codiceScheda;
    }

    /**
     * 
     * @return Codice dell'azienda.
     */
    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    /**
     * 
     * @return Codice della coltura.
     */
    public int getCodiceColtura() {
        return codiceColtura;
    }

    /**
     * 
     * @return Anno di riferimento
     */
    public int getAnno() {
        return this.anno;
    }

    /**
     * 
     * @param r Riga da aggiungere alla scheda
     */
    public void aggiungiRiga(final RigaScheda r) {
        righe.add(r);
    }

    /**
     * 
     * @return righe della scheda
     */
    public List<RigaScheda> getRighe() {
        return righe;
    }

    /**
     * 
     * @param codice
     */
    public void setCodiceScheda(final int codice) {
        this.codiceScheda = codice;
    }

}
