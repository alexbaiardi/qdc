package entities;

import java.sql.Date;

public class Prodotto {
    private final String nome;
    private String principioAttivo;
    private Date dataRevoca;

    public Prodotto(final String nome, final String principioAttivo, final Date dataRevoca) {
        this.nome = nome;
        this.principioAttivo = principioAttivo;
        this.dataRevoca = dataRevoca;
    }

    public Prodotto(final String nome, final String principioAttivo) {
        this(nome, principioAttivo, null);
    }

    /**
     * 
     * @return nome
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * 
     * @return principioAttivo
     */
    public String getPrincipioAttivo() {
        return principioAttivo;
    }

    /**
     * 
     * @param principioAttivo
     */
    public void setPrincipioAttivo(final String principioAttivo) {
        this.principioAttivo = principioAttivo;
    }
    /**
     * 
     * @return dataRevoca
     */
    public Date getDataRevoca() {
        return dataRevoca;
    }

    /**
     * 
     * @param dataRevoca
     */
    public void setDataRevoca(final Date dataRevoca) {
        this.dataRevoca = dataRevoca;
    }
}
