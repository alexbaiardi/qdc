package view;

import java.net.URL;
import java.util.ResourceBundle;

import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import entities.Azienda;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javafx.fxml.Initializable;

public class InserimentoAziendaImpl implements Initializable {
    private static final int MAXCAP = 5;
    private static final int MAXCODICES = 6;
    private static final int MAXPARTITAIVA = 11;

    @FXML
    private TextField tfCodiceSocio;

    @FXML
    private TextField tfPartitaIva;

    @FXML
    private TextField tfResponsabile;

    @FXML
    private TextField tfCap;

    @FXML
    private TextField tfProvincia;

    @FXML
    private TextField tfComune;

    @FXML
    private TextField tfCivico;

    @FXML
    private TextField tfVia;

    @FXML
    private TextField tfNomeAzienda;

    @FXML
    private Button btInserisci;
    private GestoreAziende gestoreA;

    /**
     * 
     * @param event
     */
    @FXML
    void inserisciAzienda(final ActionEvent event) {
       if (checkField()) {
            int civico = Integer.parseInt(tfCivico.getText());
            Azienda newAzienda = new Azienda(tfCodiceSocio.getText(), tfPartitaIva.getText(),
                    tfNomeAzienda.getText(), tfResponsabile.getText(), tfCap.getText(), tfProvincia.getText(),
                    tfComune.getText(), tfVia.getText(), civico);
            gestoreA.inserisciAzienda(newAzienda);
       }
    }
    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        gestoreA = new GestoreAziendeImpl();
    }

    private boolean checkField() {
        String errorMessage = "";
        if (tfCodiceSocio.getText().length() != MAXCODICES || gestoreA.checkid(tfCodiceSocio.getText())) {
            errorMessage = "CodiceSocio non valido";
            System.out.println(errorMessage);
        } else {
            if (tfPartitaIva.getText().length() != MAXPARTITAIVA) {
                errorMessage = "Numero di caratteri partitaIVA non valido";
                System.out.println(errorMessage);
            } else {
                if (tfCap.getText().length() != MAXCAP) {
                    errorMessage = "CAP non valido";
                    System.out.println(errorMessage);
                } else {
                    if (tfResponsabile.getText().isBlank() || tfProvincia.getText().isBlank()
                            || tfComune.getText().isBlank() || tfCivico.getText().isBlank() || tfVia.getText().isBlank()
                            || tfNomeAzienda.getText().isBlank()) {
                        errorMessage = "E' necessario inserire tutti i valori";
                        System.out.println(errorMessage);
                    }
                }
            }
        }
        return errorMessage == "";
    }
}
