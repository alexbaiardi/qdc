package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuButton;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

public class ViewAmministrazione extends AbstractSceneController implements Initializable {

    @FXML
    private AnchorPane apPane;

    @FXML
    private MenuButton mbSelezionaOperazione;

    @FXML
    private Button btHome;

    /**
     * 
     */
    @FXML
    public void inserisciNuovaAzienda() {
        loadContent("inserimentoAzienda.fxml");
    }

    /**
     * 
     */
    @FXML
    public void inserisciScheda() {
        loadContent("inserimentoScheda.fxml");
    }

    /**
     * 
     */
    @FXML
    public void aggiornaScheda() {
        loadContent("aggiornaScheda.fxml");
    }

    /**
     * 
     */
    @FXML
    public void visualizzaAziende() {
        loadContent("visualizzazioneAziende.fxml");
    }

    /**
     * 
     */
    @FXML
    public void apriHomePage() {
        final HomePage controller = (HomePage) this.getSceneFactory().openNewScene("home.fxml");
        controller.setSceneFactory(this.getSceneFactory());
    }

    private void loadContent(final String path) {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(path));
        try {
            apPane.getChildren().clear();
            apPane.getChildren().add(loader.load());
        } catch (IOException e) {
            final Alert alert = new Alert(AlertType.ERROR);
            e.printStackTrace();
            alert.setHeaderText("LOADING ERROR");
            alert.show();
        }
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
    }

}
