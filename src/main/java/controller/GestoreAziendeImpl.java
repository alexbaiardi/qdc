package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Azienda;

/**
 *
 */
public class GestoreAziendeImpl implements GestoreAziende {

    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreAziendeImpl() {
        dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Azienda> getAziende() {
        final List<Azienda> aziende = new ArrayList<Azienda>();
        final String query = "SELECT * FROM AZIENDE";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String codiceSocio = res.getString("codiceSocio");
                final String partitaIVA = res.getString("partitaIVA");
                final String nome = res.getString("nome");
                final String responsabile = res.getString("responsabile");
                final String cap = res.getString("cap");
                final String provincia = res.getString("provincia");
                final String comune = res.getString("comune");
                final String via = res.getString("via");
                final int civico = res.getInt("civico");
                final Azienda azienda = new Azienda(codiceSocio, partitaIVA, nome, responsabile, cap, provincia, comune,
                        via, civico);
                aziende.add(azienda);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aziende;
    }

    /**
     * 
     */
    @Override
    public void inserisciAzienda(final Azienda a) {
        final String query = "INSERT INTO aziende (codiceSocio, partitaIVA, nome, responsabile, CAP, provincia,"
                + " comune, via, civico) VALUES (  " + a.getCodiceSocio() + " , " + a.getPartitaiva() + ",' "
                + a.getNome() + "' , '" + a.getResponsabile() + "' , " + a.getCap() + ", ' " + a.getProvincia()
                + "' , ' " + a.getComune() + "' , '" + a.getVia() + "' ," + a.getCivico() + ");";
        System.out.println(query);
        dataSource.executeQuery(query);
    }

    /**
     * @param primaryKey
     * @return ascend
     */
    @Override
    public Azienda getAziendaFromPrimaryKey(final String primaryKey) {
        Azienda azienda = null;
        final String query = "SELECT * FROM AZIENDE WHERE codiceSocio ='" + primaryKey + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            if (res.next()) {
                final String codiceSocio = res.getString("codiceSocio");
                final String partitaIVA = res.getString("partitaIVA");
                final String nome = res.getString("nome");
                final String responsabile = res.getString("responsabile");
                final String cap = res.getString("cap");
                final String provincia = res.getString("provincia");
                final String comune = res.getString("comune");
                final String via = res.getString("via");
                final int civico = res.getInt("civico");
                azienda = new Azienda(codiceSocio, partitaIVA, nome, responsabile, cap, provincia, comune, via, civico);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return azienda;
    }

    /**
     * 
     * @param codiceAzienda
     * @return
     */
    @Override
    public boolean checkid(final String codiceAzienda) {
        final String query = "SELECT * FROM AZIENDE WHERE codiceSocio ='" + codiceAzienda + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            return res.next();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
