package view;

import entities.Azienda;

/**
 * Interfaccia che gestisce il passaggio dei dati di un'azienda.
 */
public interface DatiAzienda {

    /**
     * 
     * @return Azienda corrente
     */
    Azienda getAzienda();

    /**
     * @param azienda Azienda corrente.
     */
    void setAzienda(Azienda azienda);
}
