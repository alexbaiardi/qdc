package controller;

import java.util.List;

import entities.Scheda;
import entities.Trattamento;

/**
 * Gestore dei trattamenti.
 */
public interface GestoreTrattamenti {

    /**
     * Metodo per ottenere la lista dei trattamenti su una scheda.
     * @param s Scheda
     * @return Lista dei trattamenti
     */
    List<Trattamento> getTrattamenti(Scheda s);

    /**
     * Inserimento di un nuovo trattamento.
     * @param t Trattamento da inserire
     */
    void inserisciTrattamento(Trattamento t);
}
