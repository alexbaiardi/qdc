package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import entities.Azienda;
import entities.RigaScheda;
import entities.Scheda;

/**
 * 
 *
 */
public class GestoreSchedeImpl implements GestoreSchede {

    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreSchedeImpl() {
        this.dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Scheda> getSchede(final Azienda a) {
        final String query = "SELECT * FROM SCHEDE WHERE codiceAzienda = '" + a.getCodiceSocio() + "'";
        final List<Scheda> schede = new LinkedList<>();
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int codiceScheda = res.getInt("codiceScheda");
                final String codiceAzienda = res.getString("codiceAzienda");
                final int codiceColtura = res.getInt("codiceColtura");
                final int anno = res.getInt("anno");
                final Scheda s = new Scheda(codiceAzienda, codiceColtura, anno);
                s.setCodiceScheda(codiceScheda);
                final List<RigaScheda> righe = getRighe(s);
                righe.forEach(r -> s.aggiungiRiga(r));
                schede.add(s);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return schede;
    }
    /**
     * 
     */
    @Override
    public void inserisciScheda(final Scheda s) {
        final String query = "INSERT INTO SCHEDE ( anno, codiceAzienda, codiceColtura) VALUES(" + s.getAnno() + ","
                + s.getCodiceAzienda() + "," + s.getCodiceColtura() + ")";
        this.dataSource.executeQuery(query);
    }

    /**
     * 
     */
    @Override
    public void aggiungiRiga(final RigaScheda rs) {
        final int codiceRiga = getNextCodiceRiga(rs.getCodicescheda());

        final String query = "INSERT INTO righe_schede (codiceRiga,codiceScheda, superficie, particella, codiceColtura,progVarieta) VALUES(" + codiceRiga + ","
                + rs.getCodicescheda() + "," + rs.getSuperficie() + ",'" + rs.getParticella() + "',"
                + rs.getCodicecoltura() + "," + rs.getProgvarieta() + ")";
        this.dataSource.executeQuery(query);
    }

    /**
     * 
     */
    @Override
    public List<RigaScheda> getRighe(final Scheda s) {
        final String query = "SELECT * FROM RIGHE_SCHEDE WHERE codiceScheda = '" + s.getCodiceScheda() + "'";
        final List<RigaScheda> righe = new LinkedList<>();
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int codiceRiga = res.getInt("codiceRiga");
                final int codiceScheda = res.getInt("codiceScheda");
                final double superficie = res.getDouble("superficie");
                final String particella = res.getString("particella");
                final int codiceColtura = res.getInt("codiceColtura");
                final int progVarieta = res.getInt("progVarieta");
                righe.add(new RigaScheda(codiceRiga, codiceScheda, superficie, particella, codiceColtura, progVarieta));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return righe;
    }

    private int getNextCodiceRiga(final int codiceScheda) {
        final Scheda s = getScheda(codiceScheda);
        return s.getRighe().size() + 1;
    }

    private Scheda getScheda(final int codiceScheda) {
        final String query = "SELECT * FROM SCHEDE WHERE codiceScheda = " + codiceScheda;
        Scheda outScheda = null;
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String codiceAzienda = res.getString("codiceAzienda");
                final int codiceColtura = res.getInt("codiceColtura");
                final int anno = res.getInt("anno");
                final Scheda scheda = new Scheda(codiceAzienda, codiceColtura, anno);
                scheda.setCodiceScheda(codiceScheda);
                final List<RigaScheda> righe = getRighe(scheda);
                righe.forEach(r -> scheda.aggiungiRiga(r));
                outScheda = scheda;
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return outScheda;
    }


    /**
     * @param s
     * @return codiceScheda
     */
    public Optional<Integer> getCodiceScheda(final Scheda s) {
        GestoreAziende a = new GestoreAziendeImpl();
        List<Scheda> listS = getSchede(a.getAziendaFromPrimaryKey(s.getCodiceAzienda()));
        Optional<Scheda> ss =  listS.stream().filter(sc -> sc.getAnno() == s.getAnno() && sc.getCodiceAzienda().equals(sc.getCodiceAzienda())
               && sc.getCodiceColtura() == s.getCodiceColtura()).findFirst();
        if (ss.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(ss.get().getCodiceScheda());
    }
}
