package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import entities.Coltura;
import entities.Varieta;

/**
 *
 */
public class GestoreColtureImpl implements GestoreColture {

    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreColtureImpl() {
        this.dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Coltura> getColture() {
        final String query = "SELECT * FROM COLTURE";
        final List<Coltura> colture = new LinkedList<>();
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int codiceColtura = res.getInt("codiceColtura");
                final String nome = res.getString("nome");
                colture.add(new Coltura(codiceColtura, nome));
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return colture;
    }

    /**
     * 
     */
    @Override
    public List<Varieta> getVarieta(final Coltura c) {
        final String query = "SELECT * FROM VARIETA WHERE codiceColtura = " + c.getCodiceColtura();
        final List<Varieta> varieta = new LinkedList<>();
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int progVarieta = res.getInt("progVarieta");
                final String nome = res.getString("nome");
                final int codiceColtura = res.getInt("codiceColtura");
                varieta.add(new Varieta(progVarieta, nome, codiceColtura));
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return varieta;
    }

    /**
     * 
     */
    @Override
    public Coltura getColtura(final int codice) {
        final String query = "SELECT * FROM COLTURE WHERE codiceColtura = " + codice;
        Coltura c = null;
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            if (res.next()) {
                final String nome = res.getString("nome");
                c = new Coltura(codice, nome);
            } else {
                throw new IllegalArgumentException("Coltura non presente");
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }

    /**
     * 
     */
    @Override
    public Varieta getVarieta(final int codiceColtura, final int progVarieta) {
        final String query = "SELECT * FROM VARIETA WHERE codiceColtura = " + codiceColtura + " AND progVarieta="
                + progVarieta;
        Varieta v = null;
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            if (res.next()) {
                final String nome = res.getString("nome");
                v = new Varieta(progVarieta, nome, codiceColtura);
            } else {
                throw new IllegalArgumentException("Coltura non presente");
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return v;
    }


}
