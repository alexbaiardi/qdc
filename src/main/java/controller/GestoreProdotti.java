package controller;

import java.util.List;

import entities.Prodotto;

/**
 * Gestore dei prodotti.
 */
public interface GestoreProdotti {

    /**
     * Metodo per ottenere una lista dei prodotti.
     * @return Lista dei prodotti
     */
    List<Prodotto> getProdotti();

    /**
     * Metodo per ottenere un prodotto data la chiave del prodotto.
     * @param nomeProdotto Nome del prodotto
     * @return Prodotto
     */
    Prodotto getProdottoFromPrimaryKey(String nomeProdotto);
}
