package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Classe che gestisce l'interazione con il database.
 */
public class DBManager {

    private static String dbName = "quaderni_di_campagna";
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/" + dbName
            + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static final String DB_USER = "root";
    private static final String DB_PASS = "abcd";
    private Connection c;

    /**
     * 
     */
    public DBManager() {
        try {
            Class.forName(JDBC_DRIVER);
            c = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param query the query to execute in DB
     * @return result of query
     */
    public ResultSet executeQuery(final String query) {
        ResultSet result = null;
        try {
            final Statement s = c.createStatement();
            s.execute(query);
            result = s.getResultSet();
            // s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

}
