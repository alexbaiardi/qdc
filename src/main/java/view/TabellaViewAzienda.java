package view;

import javafx.scene.control.Button;

/**
 * 
 *
 */
public class TabellaViewAzienda {

    private final String coltura;
    private final Button visualizzaSchede;
    private final Button inserisciTrattamento;
    private final Button visualizzaTrattamenti;

    /**
     * 
     * @param coltura Coltura alla quale la scheda è riferita.
     * @param visualizzaSchede Bottone per visualizzare le righe della scheda. 
     * @param inserisciTrattamento Bottone per inserire un trattamento.
     * @param visualizzaTrattamenti Bottone per visualizzare i trattamenti sulla scheda.
     */
    public TabellaViewAzienda(final String coltura, final Button visualizzaSchede, final Button inserisciTrattamento,
            final Button visualizzaTrattamenti) {
        this.coltura = coltura;
        this.visualizzaSchede = visualizzaSchede;
        this.inserisciTrattamento = inserisciTrattamento;
        this.visualizzaTrattamenti = visualizzaTrattamenti;
    }
    /**
     * 
     * @return coltura
     */
    public String getColtura() {
        return coltura;
    }

    /**
     * 
     * @return visualizzaSchede
     */
    public Button getVisualizzaSchede() {
        return visualizzaSchede;
    }

    /**
     * 
     * @return inserisciTrattamento
     */
    public Button getInserisciTrattamento() {
        return inserisciTrattamento;
    }

    /**
     * 
     * @return visualizzaTrattamenti
     */
    public Button getVisualizzaTrattamenti() {
        return visualizzaTrattamenti;
    }

}
