package view;

import javafx.scene.control.Button;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import controller.GestoreAcquisti;
import controller.GestoreAcquistiImpl;
import entities.Acquisto;
import entities.Azienda;
import entities.Prodotto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 *
 */
public class ViewVisualizzaAcquisti extends AbstractAziendaSceneController implements Initializable {

    private final GestoreAcquisti gestoreAcquisti = new GestoreAcquistiImpl();
    private final ObservableList<TabellaViewAcquisti> data = FXCollections.observableArrayList();
    private final ObservableList<Integer> anni = FXCollections.observableArrayList();

    @FXML
    private TableView<TabellaViewAcquisti> tbAcquisti;

    @FXML
    private TableColumn<TabellaViewAcquisti, String> colFornitore;

    @FXML
    private TableColumn<TabellaViewAcquisti, String> colDataAcquisto;

    @FXML
    private TableColumn<TabellaViewAcquisti, String> colNumBolla;

    @FXML
    private TableColumn<TabellaViewAcquisti, String> colProdotto;

    @FXML
    private TableColumn<TabellaViewAcquisti, String> colQuantitaProdotto;

    @FXML
    private Button btHomeAzienda;

    @FXML
    private ComboBox<Integer> cbAnno;

    /**
     * 
     * @param event
     */
    @FXML
    void cercaPerAnno(final ActionEvent event) {
        final int anno = this.cbAnno.getValue();
        final List<Acquisto> acquisti = this.gestoreAcquisti.getAcquisti(this.getAzienda()).stream()
                .filter(a -> a.getData().toLocalDate().getYear() == anno).collect(Collectors.toList());
        this.popolaTabella(acquisti);
    }

    /**
     * 
     * @param event
     */
    @FXML
    void apriHomeAzienda(final ActionEvent event) {
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(this.getAzienda());
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    /**
     * 
     * @param azienda Azienda.
     */
    public void setAzienda(final Azienda azienda) {
        super.setAzienda(azienda);
        this.initializeDatiAcquisto();
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
    }

    private void initializeDatiAcquisto() {
        final List<Acquisto> acquisti = this.gestoreAcquisti.getAcquisti(this.getAzienda());
        this.popolaTabella(acquisti);
        final List<Integer> distinctAnni = new LinkedList<>();
        acquisti.forEach(a -> distinctAnni.add(a.getData().toLocalDate().getYear()));
        distinctAnni.stream().distinct().forEach(da -> anni.add(da));
        cbAnno.setItems(anni);
        cbAnno.getSelectionModel().select(Collections.max(anni));
    }

    private void popolaTabella(final List<Acquisto> acquisti) {
        this.data.clear();
        colNumBolla.setCellValueFactory(new PropertyValueFactory<>("numBolla"));
        colDataAcquisto.setCellValueFactory(new PropertyValueFactory<>("dataAcquisto"));
        colProdotto.setCellValueFactory(new PropertyValueFactory<>("prodotto"));
        colQuantitaProdotto.setCellValueFactory(new PropertyValueFactory<>("quantitaProdotto"));
        colFornitore.setCellValueFactory(new PropertyValueFactory<>("fornitore"));
        if (!acquisti.isEmpty()) {
            acquisti.forEach(a -> {
                final Map<Prodotto, Double> prodotti = a.getMap();
                prodotti.entrySet().stream().forEach(p -> {
                    final TabellaViewAcquisti acquisto = new TabellaViewAcquisti(a.getData(), p.getKey().getNome(),
                            p.getValue(), a.getFornitore(), a.getNumBolla());
                    data.add(acquisto);
                });
                tbAcquisti.setItems(data);
            });
        }
    }
}
