package view;

import java.net.URL;
import java.time.LocalDate;
import java.util.Map;
import java.util.ResourceBundle;
import controller.GestoreMovimenti;
import controller.GestoreMovimentiImpl;
import entities.Azienda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 *
 */
public class ViewVisualizzaGiacenze extends AbstractAziendaSceneController implements Initializable {

    private final GestoreMovimenti gestoreMovimenti = new GestoreMovimentiImpl();
    private final ObservableList<TabellaViewGiacenze> data = FXCollections.observableArrayList();

    @FXML
    private Button btHomeAzienda;

    @FXML
    private TableView<TabellaViewGiacenze> tbMovimenti;

    @FXML
    private TableColumn<TabellaViewGiacenze, String> colQuantita;

    @FXML
    private TableColumn<TabellaViewGiacenze, String> colProdotto;

    /**
     * @param azienda Azienda corrente.
     */
    public void setAzienda(final Azienda azienda) {
        super.setAzienda(azienda);
        this.initializeDatiGiacenze();
    }

    /**
     * 
     * @param event
     */
    @FXML
    void apriHomeAzienda(final ActionEvent event) {
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(this.getAzienda());
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    private void initializeDatiGiacenze() {
        final Map<String, Double> giacenze = this.gestoreMovimenti.getGiacenze(this.getAzienda(), LocalDate.now());
        colProdotto.setCellValueFactory(new PropertyValueFactory<>("nomeProdotto"));
        colQuantita.setCellValueFactory(new PropertyValueFactory<>("quantitaProdotto"));
        if (!giacenze.isEmpty()) {
            giacenze.entrySet().stream().forEach(g -> this.data.add(new TabellaViewGiacenze(g.getKey(), g.getValue())));
        }
        tbMovimenti.setItems(data);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
    }

}
