package view;

import java.sql.Date;

/**
 *
 */
public class TabellaViewAcquisti {

    private final Date dataAcquisto;
    private final String prodotto;
    private final double quantitaProdotto;
    private final String fornitore;
    private final int numBolla;

    /**
     * Costruttore.
     * 
     * @param dataAcquisto     Data acquisto
     * @param prodotto         Prodotto acquistato
     * @param quantitaProdotto Quantità di prodotto acquistato
     * @param fornitore        Fornitore del prodotto
     * @param numBolla         Numero di bolla
     */
    public TabellaViewAcquisti(final Date dataAcquisto, final String prodotto, final double quantitaProdotto,
            final String fornitore, final int numBolla) {
        this.dataAcquisto = dataAcquisto;
        this.prodotto = prodotto;
        this.quantitaProdotto = quantitaProdotto;
        this.fornitore = fornitore;
        this.numBolla = numBolla;
    }



    /**
     * 
     * @return dataAcquisto
     */
    public Date getDataAcquisto() {
        return dataAcquisto;
    }

    /**
     * 
     * @return prodotto
     */
    public String getProdotto() {
        return prodotto;
    }

    /**
     * 
     * @return quantitaProdotto
     */
    public double getQuantitaProdotto() {
        return quantitaProdotto;
    }

    /**
     * 
     * @return fornitore
     */
    public String getFornitore() {
        return fornitore;
    }

    /**
     * 
     * @return numBolla
     */
    public int getNumBolla() {
        return numBolla;
    }

}
