-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: quaderni_di_campagna
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `registrazioni_avversita`
--

DROP TABLE IF EXISTS `registrazioni_avversita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registrazioni_avversita` (
  `codiceRegistrazione` int NOT NULL,
  `codiceAvversita` int NOT NULL,
  `dosaggioVol` float DEFAULT NULL,
  `dosaggioSup` float DEFAULT NULL,
  PRIMARY KEY (`codiceRegistrazione`,`codiceAvversita`),
  KEY `FKaff_AVV` (`codiceAvversita`),
  CONSTRAINT `FKaff_AVV` FOREIGN KEY (`codiceAvversita`) REFERENCES `avversita` (`codiceAvversita`),
  CONSTRAINT `FKaff_REG` FOREIGN KEY (`codiceRegistrazione`) REFERENCES `registrazioni` (`codiceRegistrazione`),
  CONSTRAINT `DOSAGGIO` CHECK (((`dosaggioVol` is not null) or (`dosaggioSup` is not null)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrazioni_avversita`
--

LOCK TABLES `registrazioni_avversita` WRITE;
/*!40000 ALTER TABLE `registrazioni_avversita` DISABLE KEYS */;
INSERT INTO `registrazioni_avversita` VALUES (1,2,0.03,0.3),(2,4,0.15,1.5),(3,1,0.1,1.5),(3,6,0.2,2),(4,1,0.35,5.25),(5,5,0.075,1.125),(6,3,0.03,0.3),(7,4,0.33,5),(8,1,0.1,1.5),(8,6,0.15,2),(9,1,4,0.2),(10,2,0.03,0.3),(11,4,0.15,1.5),(12,1,0.1,1.5),(12,6,0.2,2),(13,5,0.075,1.125);
/*!40000 ALTER TABLE `registrazioni_avversita` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 16:05:54
