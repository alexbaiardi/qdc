package entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Trattamento {
    private int codiceTrattamento;
    private Date data;
    private double volumeAcqua;
    private double quantitaProdotto;
    private String nomeProdotto;
    private String codiceAzienda;
    private List<Integer> progRighe;
    private int idScheda;
    private int codiceAvversita;

    public Trattamento(final int codiceTrattamento, final Date data, final double volumeAcqua,
            final double quantitaProdotto, final String nomeProdotto, final String codiceAzienda,
            final int codiceAvversita) {
        this(data, volumeAcqua, quantitaProdotto, nomeProdotto, codiceAzienda, codiceAvversita);
        this.codiceTrattamento = codiceTrattamento;
    }

    public Trattamento(final Date data, final double volumeAcqua, final double quantitaProdotto,
            final String nomeProdotto, final String codiceAzienda, final int codiceAvversita) {
        this.progRighe = new ArrayList<Integer>();
        this.data = data;
        this.volumeAcqua = volumeAcqua;
        this.quantitaProdotto = quantitaProdotto;
        this.nomeProdotto = nomeProdotto;
        this.codiceAzienda = codiceAzienda;
        this.codiceAvversita = codiceAvversita;
    }

    /**
     * 
     * @return Codice identificativo del trattamento.
     */
    public int getCodiceTrattamento() {
        return codiceTrattamento;
    }

    /**
     * 
     * @param codiceTrattamento
     */
    public void setCodiceTrattamento(final int codiceTrattamento) {
        this.codiceTrattamento = codiceTrattamento;
    }

    /**
     * 
     * @return Data del trattamento.
     */
    public Date getData() {
        return data;
    }

    /**
     * 
     * @return Volume d'acqua per il trattamento.
     */
    public double getVolumeAcqua() {
        return volumeAcqua;
    }

    /**
     * 
     * @return Quantità di prodotto utilizzata.
     */
    public double getQuantitaProdotto() {
        return quantitaProdotto;
    }

    /**
     * 
     * @return Nome del prodotto utilizzato.
     */
    public String getNomeProdotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @return Codice dell'azienda che effettua il trattamento.
     */
    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    /**
     * 
     * @param idRiga
     */
    public void inserisciRiga(final int idRiga) {
        this.progRighe.add(idRiga);
    }

    /**
     * 
     * @param idScheda
     */
    public void setIdScheda(final int idScheda) {
        this.idScheda = idScheda;
    }

    /**
     * 
     * @return progrighe
     */
    public List<Integer> getRigheSchede() {
        return this.progRighe;
    }

    /**
     * 
     * @return idScheda
     */
    public int getIdScheda() {
        return this.idScheda;
    }

    /**
     * 
     * @return codice dell'avversità
     */
    public int getCodiceAvversita() {
        return codiceAvversita;
    }

}
