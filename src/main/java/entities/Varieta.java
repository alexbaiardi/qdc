package entities;

/**
 * Classe che implementa il concetto di varietà.
 */
public class Varieta {

    private int progVarieta;
    private String nome;
    private int codiceColtura;

    /**
     * Costruttore.
     * @param progVarieta Numero progressivo della varietà all'interno della coltura.
     * @param nome Nome della varietà.
     * @param codiceColtura Codice della coltura alla quale la varietà appartiene.
     */
    public Varieta(final int progVarieta, final String nome, final int codiceColtura) {
        this.progVarieta = progVarieta;
        this.nome = nome;
        this.codiceColtura = codiceColtura;
    }

    /**
     * 
     * @return Numero progressivo della varietà all'interno della coltura.
     */
    public int getProgVarieta() {
        return progVarieta;
    }

    /**
     * 
     * @return Nome della varietà.
     */
    public String getNome() {
        return nome;
    }

    /**
     * 
     * @return Codice della coltura.
     */
    public int getCodiceColtura() {
        return codiceColtura;
    }

}
