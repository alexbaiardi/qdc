package entities;

/**
 * Classe che implementa il concetto di registrazione di un prodotto su una
 * coltura.
 */
public class Registrazione {

    private int codiceRegistrazione;
    private int codiceColtura;
    private String nomeProdotto;
    private int carenza;
    private int numApplicazioni;

    /**
     * 
     * @return Codice identificativo della registrazione.
     */
    public int getCodiceRegistrazione() {
        return codiceRegistrazione;
    }

    /**
     * 
     * @param codiceRegistrazione Codice identificativo della registrazione.
     */
    public void setCodiceRegistrazione(final int codiceRegistrazione) {
        this.codiceRegistrazione = codiceRegistrazione;
    }

    /**
     * 
     * @return Codice della coltura sulla quale il prodotto è registrato.
     */
    public int getCodicecoltura() {
        return codiceColtura;
    }

    /**
     * 
     * @param codiceColtura Codice della coltura sulla quale il prodotto è
     *                      registrato.
     */
    public void setCodicecoltura(final int codiceColtura) {
        this.codiceColtura = codiceColtura;
    }

    /**
     * 
     * @return Nome del prodotto.
     */
    public String getNomeprodotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @param nomeProdotto Nome del prodotto.
     */
    public void setNomeprodotto(final String nomeProdotto) {
        this.nomeProdotto = nomeProdotto;
    }

    /**
     * 
     * @return Carenza del prodotto sulla coltura.
     */
    public int getCarenza() {
        return carenza;
    }

    /**
     * 
     * @param carenza Carenza del prodotto sulla coltura.
     */
    public void setCarenza(final int carenza) {
        this.carenza = carenza;
    }

    /**
     * 
     * @return Numero massimo di applicazioni annuali.
     */
    public int getNumapplicazioni() {
        return numApplicazioni;
    }

    /**
     * 
     * @param numApplicazioni Numero massimo di applicazioni annuali.
     */
    public void setNumapplicazioni(final int numApplicazioni) {
        this.numApplicazioni = numApplicazioni;
    }
}
