package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import entities.Prodotto;

/**
 *
 */
public class GestoreProdottiImpl implements GestoreProdotti {

    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreProdottiImpl() {
        this.dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Prodotto> getProdotti() {
        final String query = "SELECT * FROM PRODOTTI";
        final List<Prodotto> prodotti = new LinkedList<>();
        final ResultSet res = this.dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String principioAttivo = res.getString("principioAttivo");
                final String nome = res.getString("nome");
                final Date dataRevoca = res.getDate("dataRevoca");
                prodotti.add(new Prodotto(nome, principioAttivo, dataRevoca));
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodotti;
    }

    /**
     * 
     */
    @Override
    public Prodotto getProdottoFromPrimaryKey(final String nomeProdotto) {
        Prodotto prodotto = null;
        final String query = "SELECT * FROM PRODOTTI WHERE nome ='" + nomeProdotto + "'";
        final ResultSet res = dataSource.executeQuery(query);
        try {
            if (res.next()) {
                final String nome = res.getString("nome");
                final String principioAttivo = res.getString("principioAttivo");
                final Date dataRevoca = res.getDate("dataRevoca");
                prodotto = new Prodotto(nome, principioAttivo, dataRevoca);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prodotto;
    }

}
