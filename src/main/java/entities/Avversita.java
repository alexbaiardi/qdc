package entities;

public class Avversita {

    private final int codice;
    private final String nome;

    public Avversita(final int codice, final String nome) {
        this.codice = codice;
        this.nome = nome;
    }

    /**
     * 
     * @return codice avversità
     */
    public int getCodice() {
        return codice;
    }

    /**
     * 
     * @return nome avversità
     */
    public String getNome() {
        return nome;
    }

}
