package view;

/**
 * 
 *
 */
public class TabellaRigheScheda {

    private final int codiceRiga;
    private final double superficie;
    private final String particella;
    private final String varieta;

    /**
     * 
     * @param codiceRiga Codice della riga.
     * @param superficie Superficie dell'appezzamento.
     * @param particella Particella.
     * @param varieta    Varietà della coltura.
     */
    public TabellaRigheScheda(final int codiceRiga, final double superficie, final String particella,
            final String varieta) {
        this.codiceRiga = codiceRiga;
        this.superficie = superficie;
        this.particella = particella;
        this.varieta = varieta;
    }

    /**
     * 
     * @return codiceRiga
     */
    public int getCodiceRiga() {
        return codiceRiga;
    }

    /**
     * 
     * @return superficie;
     */
    public double getSuperficie() {
        return superficie;
    }

    /**
     * 
     * @return particella
     */
    public String getParticella() {
        return particella;
    }

    /**
     * 
     * @return varieta
     */
    public String getVarieta() {
        return varieta;
    }

}
