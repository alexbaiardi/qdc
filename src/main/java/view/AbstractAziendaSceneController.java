package view;

import entities.Azienda;

/**
 *
 */
public abstract class AbstractAziendaSceneController extends AbstractSceneController implements DatiAzienda {

    private Azienda azienda;

    /**
     * 
     */
    @Override
    public Azienda getAzienda() {
        return azienda;
    }

    /**
     * 
     */
    @Override
    public void setAzienda(final Azienda azienda) {
        this.azienda = azienda;
    }

}
