package view;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SceneFactoryImpl implements SceneFactory {

    private static final int SIZE = 750;
    private Stage stage;

    /**
     * 
     */
    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    /**
     * 
     */
    @Override
    public Stage getStage() {
        return stage;
    }

    /**
     * 
     */
    @Override
    public SceneController openNewScene(final String scenePath) {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(scenePath));
        Parent root;
        SceneController controller = null;
        try {
            root = loader.load();
            this.getStage().setScene(new Scene(root, SIZE, SIZE));
            controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

}
