package view;

import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import controller.GestoreMovimenti;
import controller.GestoreMovimentiImpl;
import controller.GestoreProdotti;
import controller.GestoreProdottiImpl;
import entities.Azienda;
import entities.Movimento;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 *
 */
public class ViewVisualizzaMovimenti extends AbstractAziendaSceneController implements Initializable {

    private final GestoreMovimenti gestoreMovimenti = new GestoreMovimentiImpl();
    private final GestoreProdotti gestoreProdotti = new GestoreProdottiImpl();
    private final ObservableList<TabellaViewMovimenti> data = FXCollections.observableArrayList();
    private final ObservableList<Integer> anni = FXCollections.observableArrayList();
    private final ObservableList<String> prodotti = FXCollections.observableArrayList();

    @FXML
    private TableView<TabellaViewMovimenti> tbMovimenti;

    @FXML
    private TableColumn<TabellaViewMovimenti, String> colDataMovimento;

    @FXML
    private TableColumn<TabellaViewMovimenti, String> colQuantita;

    @FXML
    private TableColumn<TabellaViewMovimenti, String> colProdotto;

    @FXML
    private ComboBox<String> cbProdotto;

    @FXML
    private ComboBox<Integer> cbAnno;

    @FXML
    private Button btHomeAzienda;

    @FXML
    private Button btVisualizza;

    /**
     * @param azienda Azienda corrente.
     */
    public void setAzienda(final Azienda azienda) {
        super.setAzienda(azienda);
        this.initializeDatiMovimenti();
    }

    /**
     * 
     * @param event
     */
    @FXML
    void visualizza(final ActionEvent event) {
        List<Movimento> movimenti = new LinkedList<>();
        if (!(this.cbAnno.getValue() != null)  && this.cbProdotto.getValue() != null) {
            movimenti = this.gestoreMovimenti.getMovimentiAziendabyProd(this.getAzienda(),
                    this.gestoreProdotti.getProdottoFromPrimaryKey(this.cbProdotto.getValue()));
        } else if (this.cbAnno.getValue() != null && this.cbProdotto.getValue() == null) {
            movimenti = this.gestoreMovimenti.getMovimentiAziendabyAnno(this.getAzienda(),
                    this.cbAnno.getValue());
        } else if (this.cbAnno.getValue() != null && this.cbProdotto.getValue() != null) {
            movimenti = this.gestoreMovimenti.getMovimentiAziendabyProdAndAnno(this.getAzienda(),
                    this.gestoreProdotti.getProdottoFromPrimaryKey(this.cbProdotto.getValue()), this.cbAnno.getValue());
        }
        this.popolaTabella(movimenti);

    }

    /**
     * 
     * @param event
     */
    @FXML
    void apriHomeAzienda(final ActionEvent event) {
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(this.getAzienda());
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    private void initializeDatiMovimenti() {
        final List<Movimento> movimenti = this.gestoreMovimenti.getMovimentiAzienda(this.getAzienda());
        this.popolaTabella(movimenti);
        final List<Integer> distinctAnni = new LinkedList<>();
        final List<String> distinctProdotti= new LinkedList<>();
        movimenti.forEach(m -> distinctAnni.add(m.getData().toLocalDate().getYear()));
        movimenti.forEach(m -> distinctProdotti.add(m.getNomeProdotto()));
        distinctAnni.stream().distinct().forEach(da -> anni.add(da));
        distinctProdotti.stream().distinct().forEach(dp -> prodotti.add(dp));
        cbAnno.setItems(anni);
        cbProdotto.setItems(prodotti);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
    }

    /**
     * Popola la tabella.
     * 
     * @param movimenti Lista dei movimenti
     */
    private void popolaTabella(final List<Movimento> movimenti) {
        this.data.clear();
        colDataMovimento.setCellValueFactory(new PropertyValueFactory<>("dataMovimento"));
        colProdotto.setCellValueFactory(new PropertyValueFactory<>("nomeProdotto"));
        colQuantita.setCellValueFactory(new PropertyValueFactory<>("quantita"));
        if (!movimenti.isEmpty()) {
            movimenti.forEach(m -> {
                final TabellaViewMovimenti movimento = new TabellaViewMovimenti(m.getData(), m.getNomeProdotto(),
                        m.getQuantita());
                this.data.add(movimento);
            });
        }
        tbMovimenti.setItems(data);
    }
}
