package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import entities.Acquisto;
import entities.Azienda;
import entities.Prodotto;

/**
 *
 */
public class GestoreAcquistiImpl implements GestoreAcquisti {
    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreAcquistiImpl() {
        dataSource = new DBManager();
    }

    /**
     * @param az
     * @return accost
     */
    @Override
    public List<Acquisto> getAcquisti(final Azienda az) {
        final List<Acquisto> acquisti = new ArrayList<Acquisto>();
        final String query = "SELECT * FROM ACQUISTI A JOIN COMPOSIZIONE_ACQUISTI CA ON A.codiceAcquisto = CA.codiceAcquisto"
                + " JOIN PRODOTTI P ON P.nome = CA.nomeProdotto WHERE A.codiceAzienda = '" + az.getCodiceSocio() + "'";
        final ResultSet res = dataSource.executeQuery(query);
        final GestoreProdotti p = new GestoreProdottiImpl();
        try {
            while (res.next()) {
                final int codiceSocio = res.getInt("codiceAcquisto");
                final int numBolla = res.getInt("numBolla");
                final Date data = res.getDate("data");
                final String fornitore = res.getString("fornitore");
                final String codiceAzienda = res.getString("codiceAzienda");
                final String prodotto = res.getString("nome");
                final Double quantita = res.getDouble("quantita");
                final Acquisto acquisto = new Acquisto(codiceSocio, numBolla, data, fornitore, codiceAzienda);
                acquisto.aggiungiRigaAcquisto(p.getProdottoFromPrimaryKey(prodotto), quantita);
                acquisti.add(acquisto);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return acquisti;
    }

    /**
     * @param a
     */
    @Override
    public void inserisciAcquisto(final Acquisto a) {
        final String query = "INSERT INTO ACQUISTI(numBolla, data, fornitore, codiceAzienda)VALUES(" + a.getNumBolla()
                + ",'" + a.getData() + "','" + a.getFornitore() + "','" + a.getCodiceAzienda() + "')";
        dataSource.executeQuery(query);

        final int codiceAcquisto = this.getCodiceAcquisto(a);

        final Map<Prodotto, Double> map = a.getMap();
        for (final Entry<Prodotto, Double> entry : map.entrySet()) {
            final String query2 = "INSERT INTO COMPOSIZIONE_ACQUISTI(nomeProdotto, codiceAcquisto, quantita)VALUES"
                    + "('" + entry.getKey().getNome() + "'," + codiceAcquisto + "," + entry.getValue() + ")";
            dataSource.executeQuery(query2);
        }
    }


    private int getCodiceAcquisto(final Acquisto a) {
        final String query = "SELECT codiceAcquisto FROM ACQUISTI WHERE numBolla = " + a.getNumBolla() + " AND data = '"
                + a.getData() + "' AND fornitore = '" + a.getFornitore() + "' AND codiceAzienda = '"
                + a.getCodiceAzienda() + "'";
        ResultSet res = dataSource.executeQuery(query);
        try {
            res.next();
            return res.getInt("codiceAcquisto");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
