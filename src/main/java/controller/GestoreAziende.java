package controller;

import java.util.List;

import entities.Azienda;

/**
 * Gestore delle aziende.
 */
public interface GestoreAziende {

    /**
     * Metodo per ottenere una lista delle aziende associate alla cooperativa.
     * @return Lista delle aziende
     */
    List<Azienda> getAziende();

    /**
     * Metodo per inserire una nuova azienda.
     * @param a Azienda
     */
    void inserisciAzienda(Azienda a);

    /**
     * Metodo per ottenere un'azienda data la chiave.
     * @param primaryKey Codice dell'azienda
     * @return Azienda
     */
    Azienda getAziendaFromPrimaryKey(String primaryKey);

    /**
     * Controllo per l'accesso di un'azienda.
     * @param codiceAzienda Codice dell'azienda
     * @return True se il codice è valido.
     */
    boolean checkid(String codiceAzienda);
}
