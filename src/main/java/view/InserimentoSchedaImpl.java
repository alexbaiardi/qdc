package view;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import controller.GestoreColture;
import controller.GestoreColtureImpl;
import controller.GestoreSchede;
import controller.GestoreSchedeImpl;
import entities.Azienda;
import entities.Coltura;
import entities.RigaScheda;
import entities.Scheda;
import entities.Varieta;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

public class InserimentoSchedaImpl implements Initializable {

    @FXML
    private AnchorPane pane;

    @FXML
    private ComboBox<String> cbAzienda;

    @FXML
    private ComboBox<String> cbColtura;

    @FXML
    private ComboBox<Integer> cbAnno;

    @FXML
    private ComboBox<String> cbVarieta;

    @FXML
    private TextField txtParticella;

    @FXML
    private TextField txtSuperfice;

    @FXML
    private Button btInsericiScheda;

    private Azienda azienda = null;
    private Coltura coltura = null;
    private Integer anno = null;
    private Varieta varieta = null;
    private List<Azienda> aziende;
    private List<Coltura> colture;
    private List<Varieta> lVarieta = null;
    private GestoreColture gestoreC;
    private static final Set<Integer> YEARS = Set.of(2020, 2019, 2018, 2017, 2016);

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        gestoreC = new GestoreColtureImpl();
        GestoreAziende gestoreA = new GestoreAziendeImpl();
        aziende = gestoreA.getAziende();
        aziende.forEach(e -> cbAzienda.getItems().add(e.getNome()));

        colture = gestoreC.getColture();
        colture.forEach(e -> cbColtura.getItems().add(e.getNome()));

        cbAnno.getItems().addAll(YEARS);
        cbAzienda.setOnAction(e -> getAzienda());
        cbColtura.setOnAction(e -> getColtura());
        cbAnno.setOnAction(e -> getAnno());
        cbVarieta.setOnAction(e -> getVarieta());
    }

    private void getVarieta() {
        try {
            varieta = lVarieta.get(cbVarieta.getSelectionModel().getSelectedIndex());
        } catch (Exception e) { 
        }
    }

    private void getAnno() {
        anno = cbAnno.getValue();
    }

    private void getColtura() {
        coltura = colture.get(cbColtura.getSelectionModel().getSelectedIndex());
        cbVarieta.getItems().clear();
        lVarieta = new ArrayList<Varieta>();
        lVarieta = gestoreC.getVarieta(coltura);
        lVarieta.forEach(e -> cbVarieta.getItems().add(e.getNome()));
    }

    private void getAzienda() {
        azienda = aziende.get(cbAzienda.getSelectionModel().getSelectedIndex());
    }

    /**
     * 
     * @param event
     */
    @FXML
    void inserisciScheda(final ActionEvent event) {
        if (checkField()) {
            GestoreSchede gestoreS = new GestoreSchedeImpl();
            Scheda newScheda = new Scheda(azienda.getCodiceSocio(), coltura.getCodiceColtura(), anno);
            if (gestoreS.getCodiceScheda(newScheda).isEmpty()) {
                gestoreS.inserisciScheda(newScheda);
                gestoreS.aggiungiRiga(new RigaScheda(gestoreS.getCodiceScheda(newScheda).get(), Double.parseDouble(txtSuperfice.getText()), txtParticella.getText(),
                        coltura.getCodiceColtura(), varieta.getProgVarieta()));
                loadAggiornamento();
            } else {
                System.out.println("Impossibile creare la nuova scheda, scheda gia presente");
            }
        }
    }

    private boolean checkField() {
        String errorMessage = "";
        if (txtParticella.getText().isBlank() || txtSuperfice.getText().isBlank()) {
            errorMessage = "Impossibile lasciare campi vuoti";
            System.out.println(errorMessage);
        }
        return errorMessage == "";
    }

    /**
     * 
     */
    private void loadAggiornamento() {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("aggiornaScheda.fxml"));
        try {
            pane.getChildren().clear();
            pane.getChildren().add(loader.load());
        } catch (Exception e) {
            final Alert alert = new Alert(AlertType.ERROR);
            e.printStackTrace();
            alert.setHeaderText("LOADING ERROR");
            alert.show();
        }
    }
}
