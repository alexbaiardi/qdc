package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * Provides method to initialize application.
 */
public class MainView extends Application {

    /**
     * The main of the application.
     * @param args
     *              arguments
     */
    public static void main(final String[] args) {
        launch(args);
    }

    /**
     * 
     */
    @Override
    public void start(final Stage stage) {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("home.fxml"));
        Parent root;
        try {
            root = loader.load();
            final HomePage h = loader.getController();
            final SceneFactory sceneFactory = new SceneFactoryImpl();
            sceneFactory.setStage(stage);
            h.setSceneFactory(sceneFactory);
            final Scene scene = new Scene(root, 1000, 750);
            stage.setTitle("Quaderni di campagna");
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            final Alert alert = new Alert(AlertType.ERROR);
            e.printStackTrace();
            alert.setHeaderText("LOADING ERROR");
            alert.show();
        }
    }


}
