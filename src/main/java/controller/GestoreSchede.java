package controller;

import java.util.List;
import java.util.Optional;

import entities.Azienda;
import entities.RigaScheda;
import entities.Scheda;

/**
 * Gestore delle schede.
 */
public interface GestoreSchede {

    /**
     * Metodo per ottenere le schede di un'azienda.
     * @param a Azienda
     * @return Lista delle schede
     */
    List<Scheda> getSchede(Azienda a);

    /**
     * Inserimento di una scheda.
     * @param s Scheda da inserire
     */
    void inserisciScheda(Scheda s);

    /**
     * Aggiunta di una riga alla scheda.
     * @param rs Riga da aggiungere
     */
    void aggiungiRiga(RigaScheda rs);

    /**
     * Metodo per ottenere le righe di una scheda.
     * @param s Scheda 
     * @return Lista delle righe
     */
    List<RigaScheda> getRighe(Scheda s);

    /**
     * Metodo per ottenere il codice della scheda.
     * @param s Scheda
     * @return Codice della scheda
     */
    Optional<Integer> getCodiceScheda(Scheda s);
}
