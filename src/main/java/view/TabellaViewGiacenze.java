package view;

/**
 * 
 */
public class TabellaViewGiacenze {

    private final String nomeProdotto;
    private final Double quantitaProdotto;

    /**
     * Costruttore.
     * 
     * @param nomeProdotto     Nome del prodotto.
     * @param quantitaProdotto Quantità in magazzino.
     */
    public TabellaViewGiacenze(final String nomeProdotto, final Double quantitaProdotto) {
        this.nomeProdotto = nomeProdotto;
        this.quantitaProdotto = quantitaProdotto;
    }

    /**
     * 
     * @return nomeProdotto
     */
    public String getNomeProdotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @return quantitaProdotto
     */
    public Double getQuantitaProdotto() {
        return quantitaProdotto;
    }
}
