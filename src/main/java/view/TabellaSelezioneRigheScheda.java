package view;

import javafx.scene.control.CheckBox;

/**
 *
 */
public class TabellaSelezioneRigheScheda {

    private final int codiceRiga;
    private final String varieta;
    private final String particella;
    private final double superficie;
    private final CheckBox seleziona;

    /**
     * 
     * @param codiceRiga Codice della riga
     * @param varieta    Varietà alla quale la riga si riferisce
     * @param particella Particella
     * @param superficie Superficie dell'appezzamento
     * @param seleziona  Checkbox per selezionare la riga
     */
    public TabellaSelezioneRigheScheda(final int codiceRiga, final String varieta, final double superficie,
            final String particella, final CheckBox seleziona) {
        super();
        this.codiceRiga = codiceRiga;
        this.varieta = varieta;
        this.superficie = superficie;
        this.particella = particella;
        this.seleziona = seleziona;
    }

    /**
     * 
     * @return Superficie dell'appezzamento
     */
    public double getSuperficie() {
        return this.superficie;
    }

    /**
     * 
     * @return codiceRiga
     */
    public int getCodiceRiga() {
        return codiceRiga;
    }

    /**
     * 
     * @return varieta
     */
    public String getVarieta() {
        return varieta;
    }

    /**
     * 
     * @return particella
     */
    public String getParticella() {
        return particella;
    }

    /**
     * 
     * @return seleziona
     */
    public CheckBox getSeleziona() {
        return seleziona;
    }

    /**
     * 
     * @return True se la checkBox è selezionata
     */
    public boolean isChecked() {
        return this.seleziona.isSelected();
    }

}
