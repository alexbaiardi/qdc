package view;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import controller.GestoreAcquisti;
import controller.GestoreAcquistiImpl;
import controller.GestoreProdotti;
import controller.GestoreProdottiImpl;
import entities.Acquisto;
import entities.Prodotto;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class ViewInserisciAcquisto extends AbstractAziendaSceneController implements Initializable {

    private static final int P_IVA = 11;
    private static final int NGIORNI = 31;
    private static final int NMESI = 12;

    @FXML
    private ComboBox<Integer> cbMese;

    @FXML
    private Button btHomeAzienda;

    @FXML
    private Button btAggiuntaMultipla;

    @FXML
    private TextField tfFornitore;

    @FXML
    private TextField tfNumBolla;

    @FXML
    private ComboBox<String> cbProdotto;

    @FXML
    private TextField tfQuantita;

    @FXML
    private ComboBox<Integer> cbAnno;

    @FXML
    private Button btSalvaInserimento;

    @FXML
    private ComboBox<Integer> cbGiorno;

    private Optional<Acquisto> acquisto = Optional.empty();

    private List<Prodotto> prodotti;

    /**
     * 
     */
    @FXML
    public void aggiungiProdotto() {
        Alert a = new Alert(AlertType.INFORMATION);
        String errorMessage = "";
        try {
            int numBolla = 0;
            double quantita = 0;
            String fornitore = "";
            LocalDate data = null;
            if (acquisto.isEmpty()) {
                data = LocalDate.of(cbAnno.getValue(), cbMese.getValue(), cbGiorno.getValue());
                if (!tfNumBolla.getText().isBlank()) {
                    numBolla = Integer.parseInt(tfNumBolla.getText());
                } else {
                    errorMessage = errorMessage + "Inserire numero bolla\n";
                }
                if (tfFornitore.getText().length() == P_IVA) {
                    fornitore = tfFornitore.getText();
                } else {
                    errorMessage = errorMessage + "Inserire PartitaIva Fornitore valida\n";
                }
            }
            if (cbProdotto.getValue().isBlank()) {
                errorMessage = errorMessage + "Selezionare un prodotto\n";
            }
            if (tfQuantita.getText().isBlank()) {
                errorMessage = errorMessage + "Inserire la quantità\n";
            } else {
                quantita = Double.parseDouble(tfQuantita.getText());
                if (quantita <= 0) {
                    errorMessage = errorMessage + "Inserire una quantità valida\n";
                }
            }
            if (errorMessage.isBlank()) {
                if (acquisto.isEmpty()) {
                    acquisto = Optional.of(new Acquisto(numBolla, Date.valueOf(data), fornitore,
                            this.getAzienda().getCodiceSocio()));
                    cbAnno.setDisable(true);
                    cbMese.setDisable(true);
                    cbGiorno.setDisable(true);
                    tfNumBolla.setDisable(true);
                    tfFornitore.setDisable(true);
                }
                acquisto.get().aggiungiRigaAcquisto(prodotti.get(cbProdotto.getItems().indexOf(cbProdotto.getValue())), quantita);
                tfQuantita.setText("");
                a.setHeaderText("Acquisto Inserito");
            } else {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText(errorMessage);
            }
        } catch (Exception e) {
            a.setAlertType(AlertType.ERROR);
            a.setHeaderText(e.getMessage());
        } finally {
            a.show();
        }
    }

    /**
     * 
     */
    @FXML
    public void salvaAcquisto() {
        if (acquisto.isPresent()) {
            final GestoreAcquisti ga = new GestoreAcquistiImpl();
            ga.inserisciAcquisto(acquisto.get());
            acquisto = Optional.empty();
            cbAnno.setValue(null);
            cbAnno.setDisable(false);
            cbMese.setValue(null);
            cbMese.setDisable(false);
            cbGiorno.setValue(null);
            cbGiorno.setDisable(false);
            tfNumBolla.setDisable(false);
            tfNumBolla.setText("");
            tfFornitore.setDisable(false);
            tfFornitore.setText("");
        }

    }

    /**
     * 
     */
    @FXML
    public void apriHomeAzienda() {
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(this.getAzienda());
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        cbGiorno.getItems().addAll(Stream.iterate(1, e -> e + 1).limit(NGIORNI).collect(Collectors.toList()));
        cbMese.getItems().addAll(Stream.iterate(1, e -> e + 1).limit(NMESI).collect(Collectors.toList()));
        cbAnno.getItems().addAll(Year.now().getValue(), Year.now().getValue() - 1, Year.now().getValue() - 2);
        cbAnno.setValue(LocalDate.now().getYear());
        cbMese.setValue(LocalDate.now().getMonth().getValue());
        cbGiorno.setValue(LocalDate.now().getDayOfMonth());
        final GestoreProdotti gp = new GestoreProdottiImpl();
        prodotti = gp.getProdotti();
        cbProdotto.getItems().addAll(prodotti.stream().map(p -> p.getNome()).collect(Collectors.toList()));
    }

}
