package view;

public interface SceneController {

    void setSceneFactory(SceneFactory sceneFactory);

    SceneFactory getSceneFactory();
}
