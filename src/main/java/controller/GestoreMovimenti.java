package controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import entities.Azienda;
import entities.Movimento;
import entities.Prodotto;

/**
 * Gestore dei movimenti.
 */
public interface GestoreMovimenti {

    /**
     * Metodo per ottenere i movimenti di un'azienda.
     * 
     * @param a Azienda.
     * @return Lista dei movimenti
     */
    List<Movimento> getMovimentiAzienda(Azienda a);

    /**
     * Metodo per ottenere i movimenti di un prodotto all'interno di un'azienda.
     * @param a Azienda
     * @param p Prodotto
     * @return Lista dei movimenti
     */
    List<Movimento> getMovimentiAziendabyProd(Azienda a, Prodotto p);

    /**
     * Metodo per ottenere i movimenti di un'azienda nell'anno selezionato.
     * @param a Azienda
     * @param anno Anno 
     * @return Lista dei movimenti
     */
    List<Movimento> getMovimentiAziendabyAnno(Azienda a, int anno);

    /**
     * Metodo per ottenere i movimenti di un prodotto all'interno di un'azienda nell'anno selezionato.
     * @param a Azienda
     * @param p Prodotto
     * @param anno Anno
     * @return Lista dei movimenti
     */
    List<Movimento> getMovimentiAziendabyProdAndAnno(Azienda a, Prodotto p, int anno);

    /**
     * Metodo per ottenere l'elenco dei prodotti con le relative giacenze.
     * @param a Azienda
     * @param data Data selezionata
     * @return Mappa nome dei prodotti-Giacenza
     */
    Map<String, Double> getGiacenze(Azienda a, LocalDate data);

    /**
     * Metodo per ottenere la giacenza di un prodotto aggiornata alla data selezionata.
     * @param a Azienda
     * @param p Prodotto
     * @param data Data 
     * @return Giacenza del prodotto
     */
    double getGiacenzeProdotto(Azienda a, Prodotto p, LocalDate data);
}
