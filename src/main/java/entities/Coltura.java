package entities;

/**
 *
 */
public class Coltura {
    private int codiceColtura;
    private String nome;

    /**
     * Costruttore.
     * @param codiceColtura Codice identificativo della coltura.
     * @param nome Nome della coltura
     */
    public Coltura(final int codiceColtura, final String nome) {
        this.codiceColtura = codiceColtura;
        this.nome = nome;
    }

    /**
     * 
     * @return nome
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * 
     * @return Codice identificativo della coltura.
     */
    public int getCodiceColtura() {
        return this.codiceColtura;
    }
}
