package view;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import controller.GestoreColture;
import controller.GestoreColtureImpl;
import controller.GestoreSchede;
import controller.GestoreSchedeImpl;
import entities.RigaScheda;
import entities.Scheda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 *
 */
public class ViewVisualizzaScheda extends AbstractSchedaSceneController implements Initializable {

    //private Scheda scheda;
    private final GestoreSchede gestoreSchede = new GestoreSchedeImpl();
    private final GestoreColture gestoreColture = new GestoreColtureImpl();
    private final ObservableList<TabellaRigheScheda> data = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TabellaRigheScheda, String> colSuperficie;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colVarieta;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colCodiceRiga;

    @FXML
    private TableColumn<TabellaRigheScheda, String> colParticella;

    @FXML
    private Label lblAnno;

    @FXML
    private TableView<TabellaRigheScheda> tbScheda;

    @FXML
    private Label lblColtura;

    /**
     * 
     * @param event
     */
    @FXML
    void apriHomeAzienda(final ActionEvent event) {
        final GestoreAziende ga = new GestoreAziendeImpl();
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(ga.getAziendaFromPrimaryKey(this.getScheda().getCodiceAzienda()));
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    /**
     * 
     * @param scheda Scheda.
     */
    public void setScheda(final Scheda scheda) {
        super.setScheda(scheda);
        this.initializeDatiRighe();
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        // TODO Auto-generated method stub

    }

    private void initializeDatiRighe() {
        colCodiceRiga.setCellValueFactory(new PropertyValueFactory<>("codiceRiga"));

        colVarieta.setCellValueFactory(new PropertyValueFactory<>("varieta"));

        colParticella.setCellValueFactory(new PropertyValueFactory<>("particella"));

        colSuperficie.setCellValueFactory(new PropertyValueFactory<>("superficie"));

        final List<RigaScheda> righe = this.gestoreSchede.getRighe(this.getScheda());
        if (!righe.isEmpty()) {
            righe.forEach(r -> {
                final TabellaRigheScheda riga = new TabellaRigheScheda(r.getCodiceRiga(), r.getSuperficie(),
                        r.getParticella(),
                        this.gestoreColture.getVarieta(this.getScheda().getCodiceColtura(), r.getProgvarieta()).getNome());
                data.add(riga);
            });

            this.tbScheda.setItems(data);
            this.lblAnno.setText(String.valueOf(this.getScheda().getAnno()));
            this.lblColtura.setText(this.gestoreColture.getColtura(this.getScheda().getCodiceColtura()).getNome());
        }
    }

}
