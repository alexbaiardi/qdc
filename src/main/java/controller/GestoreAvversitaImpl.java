package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Avversita;
import entities.Azienda;
import entities.Coltura;
import entities.Prodotto;

/**
 *
 */
public class GestoreAvversitaImpl implements GestoreAvversita {

    private final DBManager dataSource;

    /**
     * 
     */
    public GestoreAvversitaImpl() {
        dataSource = new DBManager();
    }

    /**
     * 
     */
    @Override
    public List<Avversita> getAvversita(final Coltura c) {
        final List<Avversita> avversita = new ArrayList<>();
        final String query = "SELECT * FROM AVVERSITA A JOIN AVVERSITA_COLTURE AC ON A.codiceAvversita = AC.codiceAvversita "
                + "WHERE AC.codiceColtura = " + c.getCodiceColtura();
        ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int codice = res.getInt("codiceAvversita");
                final String nome = res.getString("nome");
                final Avversita av = new Avversita(codice, nome);
                avversita.add(av);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return avversita;
    }

    /**
     * 
     */
    @Override
    public Map<Prodotto, Double> getProdottiAvversitaColturaAzienda(final Avversita av, final Coltura c, final Azienda az, final LocalDate data) {
        final Map<Prodotto, Double> prodotti = new HashMap<>();
        final String queryGiacenze = "SELECT P.nome AS nomeProdotto,(COALESCE(SUM(CA.quantita),0)-COALESCE(SUM(T.quantitaProdotto),0)) AS giacenza "
                + "                     FROM PRODOTTI P JOIN COMPOSIZIONE_ACQUISTI CA"
                + "                         ON P.nome = CA.nomeProdotto"
                + "                     JOIN ACQUISTI A"
                + "                         ON CA.codiceAcquisto = A.codiceAcquisto"
                + "                     LEFT JOIN TRATTAMENTI T"
                + "                         ON P.nome = T.nomeProdotto"
                + "                 WHERE A.codiceAzienda= '" + az.getCodiceSocio() + "' AND A.data<='" + Date.valueOf(data) + "' AND (T.data IS NULL OR T.data<='" + Date.valueOf(data)
                + "')                 GROUP BY nomeProdotto";
        final String query = "SELECT P.nome, P.principioAttivo, P.dataRevoca, G.giacenza "
                + "             FROM PRODOTTI P JOIN REGISTRAZIONI R "
                + "                 ON P.nome=R.NomeProdotto"
                + "             JOIN REGISTRAZIONI_AVVERSITA RA"
                + "                 ON R.codiceRegistrazione=RA.codiceRegistrazione" + ""
                + "             JOIN ( " + queryGiacenze + " ) G "
                + "                 ON P.nome=G.nomeProdotto"
                + "             WHERE RA.codiceAvversita=" + av.getCodice() + " AND R.codiceColtura=" + c.getCodiceColtura();
        ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final String principioAttivo = res.getString("principioAttivo");
                final String nome = res.getString("nome");
                final Date dataRevoca = res.getDate("dataRevoca");
                final double quantita = res.getDouble("giacenza");
                prodotti.put(new Prodotto(nome, principioAttivo, dataRevoca), quantita);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodotti;
    }

    /**
     * 
     */
    @Override
    public Avversita getAvversitaFromPrimaryKey(final int codiceAvversita) {
        Avversita avversita = null;
        final String query = "SELECT * FROM AVVERSITA WHERE codiceAvversita =" + codiceAvversita;
        final ResultSet res = dataSource.executeQuery(query);
        try {
            if (res.next()) {
                final String nome = res.getString("nome");
                final int codice = res.getInt("codiceAvversita");
                avversita = new Avversita(codice, nome);
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avversita;
    }

}
