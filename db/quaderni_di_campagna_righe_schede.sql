-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: quaderni_di_campagna
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `righe_schede`
--

DROP TABLE IF EXISTS `righe_schede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `righe_schede` (
  `codiceRiga` int NOT NULL,
  `codiceScheda` int NOT NULL,
  `superficie` float NOT NULL,
  `particella` varchar(30) NOT NULL,
  `codiceColtura` int NOT NULL,
  `progVarieta` int NOT NULL,
  PRIMARY KEY (`codiceRiga`,`codiceScheda`),
  UNIQUE KEY `IDRIGA_SCHEDA` (`codiceScheda`,`codiceRiga`),
  KEY `FKR` (`codiceColtura`,`progVarieta`),
  CONSTRAINT `FKR` FOREIGN KEY (`codiceColtura`, `progVarieta`) REFERENCES `varieta` (`codiceColtura`, `progVarieta`),
  CONSTRAINT `FKsuddivisione` FOREIGN KEY (`codiceScheda`) REFERENCES `schede` (`codiceScheda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `righe_schede`
--

LOCK TABLES `righe_schede` WRITE;
/*!40000 ALTER TABLE `righe_schede` DISABLE KEYS */;
INSERT INTO `righe_schede` VALUES (1,1,1,'12',1,1);
/*!40000 ALTER TABLE `righe_schede` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 16:05:53
