package controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import entities.Avversita;
import entities.Azienda;
import entities.Coltura;
import entities.Prodotto;

/**
 * Gestore delle avversità.
 */
public interface GestoreAvversita {

    /**
     * Metodo per ottenere la lista delle avversità che colpiscono una coltura.
     * 
     * @param c Coltura
     * @return Lista delle aversità
     */
    List<Avversita> getAvversita(Coltura c);

    /**
     * Metodo per ottenere il prodotto associato alla giacenza.
     * 
     * @param av   Avversità
     * @param c    Coltura
     * @param az   Azienda
     * @param data Data selezionata
     * @return Prodotto - Giacenza
     */
    Map<Prodotto, Double> getProdottiAvversitaColturaAzienda(Avversita av, Coltura c, Azienda az, LocalDate data);

    /**
     * Metodo per ottenere un'avversità data la chiave.
     * 
     * @param codiceAvversita Codice dell'avversità
     * @return Avversità
     */
    Avversita getAvversitaFromPrimaryKey(int codiceAvversita);

}
