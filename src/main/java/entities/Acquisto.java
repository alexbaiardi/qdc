package entities;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class Acquisto {

    private int codiceAcquisto;
    private int numBolla;
    private Date data;
    private String fornitore;
    private String codiceAzienda;
    private final Map<Prodotto, Double> rigaAcquisto = new HashMap<>();

    public Acquisto(final int codiceAcquisto, final int numBolla, final Date data, final String fornitore,
            final String codiceAzienda) {
        this(numBolla, data, fornitore, codiceAzienda);
        this.codiceAcquisto = codiceAcquisto;
    }

    public Acquisto(final int numBolla, final Date data, final String fornitore, final String codiceAzienda) {
        this.numBolla = numBolla;
        this.data = data;
        this.fornitore = fornitore;
        this.codiceAzienda = codiceAzienda;
    }


    /**
     * 
     * @param codiceAcquisto
     */
    public void setCodiceAcquisto(final int codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }

    /**
     * 
     * @return data di acquisto
     */
    public int getCodiceAcquisto() {
        return this.codiceAcquisto;
    }

    /**
     * 
     * @return numero della bolla
     */
    public int getNumBolla() {
        return numBolla;
    }

    /**
     * 
     * @return data di acquisto
     */
    public Date getData() {
        return data;
    }

    /**
     * 
     * @return PartitaIva del fornitore
     */
    public String getFornitore() {
        return fornitore;
    }

    /**
     * 
     * @return codiceAzienda
     */
    public String getCodiceAzienda() {
        return this.codiceAzienda;
    }

    /**
     * 
     * @param p
     * @param quantita
     */
    public void aggiungiRigaAcquisto(final Prodotto p, final double quantita) {
        this.rigaAcquisto.put(p, quantita);
    }

    /**
     * 
     * @return rigaAcquisto
     */
    public Map<Prodotto, Double> getMap() {
        return rigaAcquisto;
    }

}
