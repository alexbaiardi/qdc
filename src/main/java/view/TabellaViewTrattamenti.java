package view;

import java.sql.Date;
import java.util.List;

/**
 * 
 */
public class TabellaViewTrattamenti {

    private final Date data;
    private final String nomeProdotto;
    private final String avversita;
    private final Double quantitaProdotto;
    private final Double volumeAcqua;
    private final List<Integer> righe;

    /**
     * 
     * @param data Data del trattamento
     * @param nomeProdotto Nome del prodotto utilizzato
     * @param avversita Avversità contro la quale è stato effettuato il trattamento
     * @param quantitaProdotto Quantità di prodotto utilizzata
     * @param volumeAcqua Volume d'acqua utilizzata
     * @param righe Righe sul quale è stato effettuato il trattamento
     */
    public TabellaViewTrattamenti(final Date data, final String nomeProdotto, final String avversita, final Double quantitaProdotto,
            final Double volumeAcqua, final List<Integer> righe) {
        this.data = data;
        this.nomeProdotto = nomeProdotto;
        this.avversita = avversita;
        this.quantitaProdotto = quantitaProdotto;
        this.volumeAcqua = volumeAcqua;
        this.righe = righe;
    }
    /**
     * 
     * @return data
     */
    public Date getData() {
        return data;
    }

    /**
     * 
     * @return nomeProdotto
     */
    public String getNomeProdotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @return avversita
     */
    public String getAvversita() {
        return avversita;
    }

    /**
     * 
     * @return quantitaProdotto
     */
    public Double getQuantitaProdotto() {
        return quantitaProdotto;
    }

    /**
     * 
     * @return volumeAcqua
     */
    public Double getVolumeAcqua() {
        return volumeAcqua;
    }

    /**
     * 
     * @return righe
     */
    public List<Integer> getRighe() {
        return righe;
    }

}
