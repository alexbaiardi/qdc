package view;

import java.util.List;

import controller.GestoreAvversita;
import controller.GestoreAvversitaImpl;
import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import controller.GestoreTrattamenti;
import controller.GestoreTrattamentiImpl;
import entities.Scheda;
import entities.Trattamento;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 *
 */
public class ViewVisualizzaTrattamenti extends AbstractSchedaSceneController {

    @FXML
    private Button btHomeAzienda;

    @FXML
    private TableView<TabellaViewTrattamenti> tbTrattamenti;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colData;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colProdotto;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colAvversita;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colQuantita;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colVolume;

    @FXML
    private TableColumn<TabellaViewTrattamenti, String> colRighe;

    private final GestoreTrattamenti gestoreTrattamenti = new GestoreTrattamentiImpl();
    private final ObservableList<TabellaViewTrattamenti> data = FXCollections.observableArrayList();

    /**
     * 
     */
    @Override
    public void setScheda(final Scheda scheda) {
        super.setScheda(scheda);
        this.initializeDatiTrattamenti();
    }

    private void initializeDatiTrattamenti() {
        this.popolaTabella(this.gestoreTrattamenti.getTrattamenti(this.getScheda()));
    }

    /**
     * 
     * @param event
     */
    @FXML
    void apriHomeAzienda(final ActionEvent event) {
        final GestoreAziende ga = new GestoreAziendeImpl();
        final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
        controllerAzienda.setAzienda(ga.getAziendaFromPrimaryKey(this.getScheda().getCodiceAzienda()));
        controllerAzienda.setSceneFactory(this.getSceneFactory());
    }

    private void popolaTabella(final List<Trattamento> trattamenti) {
        this.data.clear();
        this.colData.setCellValueFactory(new PropertyValueFactory<>("data"));
        this.colProdotto.setCellValueFactory(new PropertyValueFactory<>("nomeProdotto"));
        this.colAvversita.setCellValueFactory(new PropertyValueFactory<>("avversita"));
        this.colQuantita.setCellValueFactory(new PropertyValueFactory<>("quantitaProdotto"));
        this.colVolume.setCellValueFactory(new PropertyValueFactory<>("volumeAcqua"));
        this.colRighe.setCellValueFactory(new PropertyValueFactory<>("righe"));

        final GestoreAvversita ga = new GestoreAvversitaImpl();
        if (!trattamenti.isEmpty()) {
            trattamenti.forEach(t -> {
                final TabellaViewTrattamenti trattamento = new TabellaViewTrattamenti(t.getData(), t.getNomeProdotto(),
                        ga.getAvversitaFromPrimaryKey(t.getCodiceAvversita()).getNome(), t.getQuantitaProdotto(),
                        t.getVolumeAcqua(), t.getRigheSchede());
                this.data.add(trattamento);
            });
            this.tbTrattamenti.setItems(data);
        }
    }
}
