package controller;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Acquisto;
import entities.Azienda;
import entities.Prodotto;
import entities.Scheda;
import entities.Trattamento;
/**
 *
 */
public class GestoreTrattamentiImpl implements GestoreTrattamenti {

    private final DBManager dataSource;
    /**
     * 
     */
    public GestoreTrattamentiImpl() {
        dataSource = new DBManager();
    }
    /**
     * 
     */
    @Override
    public List<Trattamento> getTrattamenti(final Scheda s) {
        final List<Trattamento> trattamenti = new ArrayList<Trattamento>();
        final String query = "SELECT * FROM TRATTAMENTI AS T JOIN TRATTAMENTI_RIGHE TR ON T.codiceTrattamento = TR.codiceTrattamento"
                + " JOIN RIGHE_SCHEDE RS ON TR.codiceRiga = RS.codiceRiga AND TR.codiceScheda=RS.codiceScheda WHERE RS.codiceScheda = " + s.getCodiceScheda()+" GROUP BY T.codiceTrattamento";
        ResultSet res = dataSource.executeQuery(query);
        try {
            while (res.next()) {
                final int codiceTrattamento = res.getInt("codiceTrattamento");
                final Date data = res.getDate("data");
                final double volumeAcqua = res.getDouble("volumeAcqua");
                final double quantitaProdotto = res.getDouble("quantitaProdotto");
                final String nomeProdotto = res.getString("nomeProdotto");
                final String codiceAzienda = res.getString("codiceAzienda");
                final int codiceAvversita = res.getInt("codiceAvversita");
                final Trattamento trat = new Trattamento(codiceTrattamento, data, volumeAcqua, quantitaProdotto,
                        nomeProdotto, codiceAzienda, codiceAvversita);
                trat.setIdScheda(s.getCodiceScheda());
                trattamenti.add(trat);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        trattamenti.forEach(t -> {
            final String queryRighe = "SELECT codiceRiga FROM TRATTAMENTI_RIGHE WHERE codiceTrattamento="
                    + t.getCodiceTrattamento() + " AND codiceScheda=" + t.getIdScheda();
            final ResultSet resRighe = dataSource.executeQuery(queryRighe);
            try {
                while (resRighe.next()) {
                    t.inserisciRiga(resRighe.getInt(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return trattamenti;
    }

    /**
     * 
     */
    @Override
    public void inserisciTrattamento(final Trattamento t) {
        checkDataRevoca(t);
        checkGiacenza(t);
        checkNumApplicazioni(t);
        checkDoseVolume(t);
        checkDoseSuperficie(t);
        final String query = "INSERT INTO TRATTAMENTI(data, volumeAcqua, quantitaProdotto, nomeProdotto, codiceAzienda, codiceAvversita)VALUES('"
                + t.getData() + "'," + t.getVolumeAcqua() + "," + t.getQuantitaProdotto() + ",'" + t.getNomeProdotto()
                + "'," + t.getCodiceAzienda() + "," + t.getCodiceAvversita() + ")";
        dataSource.executeQuery(query);

        final String queryId = "SELECT LAST_INSERT_ID()";
        final ResultSet resId = dataSource.executeQuery(queryId);
        try {
            if (resId.next()) {
                t.setCodiceTrattamento(resId.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        t.getRigheSchede().forEach(r -> {
            final String queryRiga = "INSERT INTO TRATTAMENTI_RIGHE (codiceScheda,codiceRiga,codiceTrattamento) VALUES ("
                    + t.getIdScheda() + "," + r + "," + t.getCodiceTrattamento() + ")";
            dataSource.executeQuery(queryRiga);
        });

    }

    private void checkDataRevoca(final Trattamento t) throws IllegalStateException {
        final GestoreProdotti gp = new GestoreProdottiImpl();
        final Prodotto p = gp.getProdottoFromPrimaryKey(t.getNomeProdotto());
        if (p.getDataRevoca() != null && t.getData().after(p.getDataRevoca())) {
            throw new IllegalStateException(
                    "Data trattamento non valida, prodotto revocato il " + p.getDataRevoca().toLocalDate() + "\n");
        }
    }

    private void checkGiacenza(final Trattamento t) throws IllegalStateException {
        final GestoreMovimenti gm = new GestoreMovimentiImpl();
        final double giacenza = gm.getGiacenzeProdotto(new Azienda(t.getCodiceAzienda()),
                new Prodotto(t.getNomeProdotto(), ""), t.getData().toLocalDate());
        if (giacenza < t.getQuantitaProdotto()) {
            throw new IllegalStateException(
                    "La quantità inserita supera la quantità in magazzino " + giacenza + " kg/l\n");
        }
    }

    private void checkNumApplicazioni(final Trattamento t) throws IllegalStateException {
        final String queryCodColtura = "SELECT codiceColtura FROM SCHEDE WHERE codiceScheda=" + t.getIdScheda();
        final String query = "SELECT numApplicazioni FROM REGISTRAZIONI WHERE codiceColtura=(" + queryCodColtura
                + ") AND nomeProdotto='" + t.getNomeProdotto() + "';";
        final ResultSet res = dataSource.executeQuery(query);
        int maxApp = 0;
        try {
            if (res.next()) {
                maxApp = res.getInt(1);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final int maxApplicazioni = maxApp;
        t.getRigheSchede().forEach(r -> {
            final String queryNumApplicazioni = "SELECT COUNT(*) FROM TRATTAMENTI T JOIN TRATTAMENTI_RIGHE TR "
                    + "ON T.codiceTrattamento=TR.codiceTrattamento" + " JOIN righe_schede RS "
                    + "ON TR.codiceRiga=RS.codiceRiga AND TR.codiceScheda=RS.codiceScheda " + "WHERE RS.codiceScheda="
                    + t.getIdScheda() + " AND RS.codiceRiga=" + r + " AND nomeProdotto='" + t.getNomeProdotto() + "';";
            int numApplicazioni = 0;
            final ResultSet resNumApplicazioni = dataSource.executeQuery(queryNumApplicazioni);
            try {
                if (resNumApplicazioni.next()) {
                    numApplicazioni = resNumApplicazioni.getInt(1);
                }
                resNumApplicazioni.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (numApplicazioni > maxApplicazioni) {
                throw new IllegalStateException(
                        "Raggiunto il numero massimo di applicazioni " + maxApplicazioni + " nella riga " + r + "\n");
            }
        });
    }

    private void checkDoseSuperficie(final Trattamento t) throws IllegalStateException {
        final String queryCodColtura = "SELECT codiceColtura FROM SCHEDE WHERE codiceScheda=" + t.getIdScheda();
        final String query = "SELECT dosaggioSup FROM REGISTRAZIONI R"
                + "             JOIN REGISTRAZIONI_AVVERSITA RA"
                + "                 ON R.codiceRegistrazione=RA.codiceRegistrazione"
                + "           WHERE R.codiceColtura=(" + queryCodColtura + ") AND RA.codiceAvversita=" + t.getCodiceAvversita();
        final ResultSet res = dataSource.executeQuery(query);
        double dosSupMax = 0;
        double superficieSel = 0;
        try {
            if (res.next()) {
                dosSupMax = res.getDouble(1);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final String righe = t.getRigheSchede().stream().map(id -> id.intValue() + ", ").reduce((x, y) -> x + y).get();
        final String querySuperficie = "SELECT SUM(superficie) FROM RIGHE_SCHEDE WHERE codiceScheda=" + t.getIdScheda()
                + " AND codiceRiga IN (" + righe.substring(0, righe.length() - 2) + ")";
        final ResultSet resSuperficeSelezionata = dataSource.executeQuery(querySuperficie);
        try {
            if (resSuperficeSelezionata.next()) {
                superficieSel = resSuperficeSelezionata.getInt(1);
            }
            resSuperficeSelezionata.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (dosSupMax < t.getQuantitaProdotto() / superficieSel) {
            throw new IllegalStateException("Superata dose massima per superficie " + dosSupMax + " kg/ha\n");
        }
    }

    private void checkDoseVolume(final Trattamento t) throws IllegalArgumentException {
        final String queryCodColtura = "SELECT codiceColtura FROM SCHEDE WHERE codiceScheda=" + t.getIdScheda();
        final String query = "SELECT dosaggioVol FROM REGISTRAZIONI R"
                + "             JOIN REGISTRAZIONI_AVVERSITA RA"
                + "                 ON R.codiceRegistrazione=RA.codiceRegistrazione"
                + "           WHERE R.codiceColtura=(" + queryCodColtura + ") AND RA.codiceAvversita=" + t.getCodiceAvversita();
        final ResultSet res = dataSource.executeQuery(query);
        double dosVolMax = 0;
        try {
            if (res.next()) {
                dosVolMax = res.getDouble(1);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (dosVolMax < t.getQuantitaProdotto() / t.getVolumeAcqua()) {
            throw new IllegalStateException("Superata dose massima per volume " + dosVolMax + " kg/hl \n");
        }

    }

}
