package view;

import java.net.URL;
import java.util.ResourceBundle;

import controller.GestoreAziende;
import controller.GestoreAziendeImpl;
import entities.Azienda;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class HomePage extends AbstractSceneController implements Initializable {

    @FXML
    private Button btnAmministrazione;

    @FXML
    private TextField txtCodiceAzienda;

    @FXML
    private Button btnAzienda;

    /**
     * 
     */
    @FXML
    public void visualizzaVistaAmministrazione() {
        final ViewAmministrazione controller = (ViewAmministrazione) this.getSceneFactory().openNewScene("amministrazione.fxml");
        controller.setSceneFactory(this.getSceneFactory());
    }

    /**
     * 
     */
    @FXML
    public void visualizzaVistaAzienda() {
        if (!txtCodiceAzienda.getText().isBlank()) {
            final GestoreAziende ga = new GestoreAziendeImpl();
            final Azienda a = ga.getAziendaFromPrimaryKey(txtCodiceAzienda.getText());
            if (a != null) {
                final ViewAzienda controllerAzienda = (ViewAzienda) this.getSceneFactory().openNewScene("azienda.fxml");
                controllerAzienda.setAzienda(a);
                controllerAzienda.setSceneFactory(this.getSceneFactory());
            } else {
                final Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText("Inserire un codice azienda valido");
                alert.show();
            }

        } else {
            final Alert a = new Alert(AlertType.ERROR);
            a.setHeaderText("Inserire un codice azienda");
            a.show();
        }
    }

    /**
     * 
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {

    }
}
