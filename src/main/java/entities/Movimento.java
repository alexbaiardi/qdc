package entities;

import java.sql.Date;

public class Movimento {

    private final String nomeProdotto;
    private final double quantita;
    private final Date data;

    public Movimento(final String nomeProdotto, final double quantita, final Date data) {
        this.nomeProdotto = nomeProdotto;
        this.quantita = quantita;
        this.data = data;
    }

    /**
     * 
     * @return nome del prodotto
     */
    public String getNomeProdotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @return quantita
     */
    public double getQuantita() {
        return quantita;
    }

    /**
     * 
     * @return data movimento
     */
    public Date getData() {
        return data;
    }

}
