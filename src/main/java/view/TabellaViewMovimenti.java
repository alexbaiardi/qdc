package view;

import java.sql.Date;

/**
 * 
 *
 */
public class TabellaViewMovimenti {

    private final Date dataMovimento;
    private final String nomeProdotto;
    private final Double quantita;

    /**
     * Costruttore.
     * 
     * @param dataMovimento Data del movimento
     * @param nomeProdotto  Nome del prodotto
     * @param quantita      Quantita di prodotto
     */
    public TabellaViewMovimenti(final Date dataMovimento, final String nomeProdotto, final Double quantita) {
        this.dataMovimento = dataMovimento;
        this.nomeProdotto = nomeProdotto;
        this.quantita = quantita;
    }

    /**
     * 
     * @return dataMovimento
     */
    public Date getDataMovimento() {
        return dataMovimento;
    }

    /**
     * 
     * @return getNomeProdotto
     */
    public String getNomeProdotto() {
        return nomeProdotto;
    }

    /**
     * 
     * @return quantità
     */
    public Double getQuantita() {
        return quantita;
    }


}
